-- MySQL dump 10.13  Distrib 8.0.19, for Win64 (x86_64)
--
-- Host: localhost    Database: rusovich
-- ------------------------------------------------------
-- Server version	8.0.19

/*!40101 SET @OLD_CHARACTER_SET_CLIENT = @@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS = @@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION = @@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE = @@TIME_ZONE */;
/*!40103 SET TIME_ZONE = '+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;

--
-- Table structure for table `brands`
--

DROP TABLE IF EXISTS `brands`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `brands`
(
    `id`   mediumint unsigned NOT NULL AUTO_INCREMENT,
    `name` varchar(45)        NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `brands`
--

LOCK TABLES `brands` WRITE;
/*!40000 ALTER TABLE `brands`
    DISABLE KEYS */;
INSERT INTO `brands`
VALUES (1, 'KAPOUS'),
       (2, 'NEXXT'),
       (3, 'BRELIL');
/*!40000 ALTER TABLE `brands`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `catalog`
--

DROP TABLE IF EXISTS `catalog`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `catalog`
(
    `id`               mediumint unsigned NOT NULL AUTO_INCREMENT,
    `status_id`        mediumint unsigned NOT NULL,
    `name`             varchar(45)        NOT NULL,
    `date_of_creation` date               NOT NULL,
    `shop_id`          mediumint unsigned DEFAULT NULL,
    `user_id`          mediumint unsigned DEFAULT NULL,
    `date_of_change`   datetime           DEFAULT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `documents_un` (`name`, `date_of_creation`),
    KEY `documents_fk` (`status_id`),
    KEY `documents_fk_1` (`shop_id`),
    KEY `documents_fk_2` (`user_id`),
    CONSTRAINT `documents_fk` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `documents_fk_1` FOREIGN KEY (`shop_id`) REFERENCES `shops` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `documents_fk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `catalog`
--

LOCK TABLES `catalog` WRITE;
/*!40000 ALTER TABLE `catalog`
    DISABLE KEYS */;
INSERT INTO `catalog`
VALUES (1, 1, 'NEXXT', '2020-03-15', 1, 1, NULL),
       (6, 4, 'KAPOUS', '2020-03-14', NULL, NULL, NULL);
/*!40000 ALTER TABLE `catalog`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `documents`
--

DROP TABLE IF EXISTS `documents`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `documents`
(
    `catalog_id` mediumint unsigned NOT NULL,
    `line`       mediumint unsigned NOT NULL,
    `product_id` mediumint unsigned NOT NULL,
    `order`      mediumint unsigned DEFAULT '0',
    UNIQUE KEY `document_un` (`catalog_id`, `line`),
    KEY `document_fk_1` (`product_id`),
    KEY `document_fk` (`catalog_id`),
    CONSTRAINT `document_fk` FOREIGN KEY (`catalog_id`) REFERENCES `catalog` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
    CONSTRAINT `document_fk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `documents`
--

LOCK TABLES `documents` WRITE;
/*!40000 ALTER TABLE `documents`
    DISABLE KEYS */;
/*!40000 ALTER TABLE `documents`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products`
(
    `id`       mediumint unsigned NOT NULL AUTO_INCREMENT,
    `barcode`  varchar(20)        NOT NULL,
    `article`  varchar(20) DEFAULT NULL,
    `name`     varchar(45)        NOT NULL,
    `price`    decimal(10, 2)     NOT NULL,
    `balance`  mediumint unsigned NOT NULL,
    `brand_id` mediumint unsigned NOT NULL,
    PRIMARY KEY (`id`),
    UNIQUE KEY `barcode_un` (`barcode`),
    KEY `products_fk` (`brand_id`),
    CONSTRAINT `products_fk` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products`
    DISABLE KEYS */;
INSERT INTO `products`
VALUES (3, '9999999999999999', 'АЗ-999', 'Шампунь', 20.18, 10, 1),
       (4, 'a6434593059243d', '634 КЗ', 'Лак', 10.13, 1, 2);
/*!40000 ALTER TABLE `products`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles`
(
    `id`   mediumint unsigned                                                                 NOT NULL AUTO_INCREMENT,
    `name` enum ('admin','manager','seller') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 4
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles`
    DISABLE KEYS */;
INSERT INTO `roles`
VALUES (1, 'admin'),
       (2, 'manager'),
       (3, 'seller');
/*!40000 ALTER TABLE `roles`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `shops`
--

DROP TABLE IF EXISTS `shops`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `shops`
(
    `id`      mediumint unsigned NOT NULL AUTO_INCREMENT,
    `name`    varchar(45)        NOT NULL,
    `address` varchar(100) DEFAULT '-address-',
    PRIMARY KEY (`id`),
    UNIQUE KEY `name_un` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `shops`
--

LOCK TABLES `shops` WRITE;
/*!40000 ALTER TABLE `shops`
    DISABLE KEYS */;
INSERT INTO `shops`
VALUES (1, 'Минский магазин', 'г. Минск, ул Минская, д.55, пав. 5');
/*!40000 ALTER TABLE `shops`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `status`
(
    `id`   mediumint unsigned                                                                                      NOT NULL AUTO_INCREMENT,
    `name` enum ('new','work','confirmed','complited','archival') CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 10
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `status`
--

LOCK TABLES `status` WRITE;
/*!40000 ALTER TABLE `status`
    DISABLE KEYS */;
INSERT INTO `status`
VALUES (1, 'work'),
       (2, 'confirmed'),
       (3, 'complited'),
       (4, 'new'),
       (5, 'archival');
/*!40000 ALTER TABLE `status`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users`
(
    `id`        mediumint unsigned NOT NULL AUTO_INCREMENT,
    `full_name` varchar(45)        NOT NULL,
    `login`     varchar(45)        NOT NULL,
    `password`  varchar(45)        NOT NULL,
    `role_id`   mediumint unsigned NOT NULL,
    `is_active` tinyint(1)         NOT NULL DEFAULT '1',
    PRIMARY KEY (`id`),
    UNIQUE KEY `full_name_un` (`full_name`),
    UNIQUE KEY `login_un` (`login`),
    KEY `users_fk` (`role_id`),
    CONSTRAINT `users_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB
  AUTO_INCREMENT = 5
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users`
    DISABLE KEYS */;
INSERT INTO `users`
VALUES (1, 'Василий Васильевич Василевич', 'vasya', '12345', 1, 1),
       (2, 'Надежда Вячеславовна Явкова', 'nadejda', '123456', 2, 1);
/*!40000 ALTER TABLE `users`
    ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'rusovich'
--
/*!40103 SET TIME_ZONE = @OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT = @OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS = @OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION = @OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;

-- Dump completed on 2020-04-05 22:54:04
