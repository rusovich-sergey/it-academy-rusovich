package by.srg.webmvc.service;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class SecurityInterceptor extends HandlerInterceptorAdapter {

    private static final Set<String> ALLOWED_PATHS = Collections.unmodifiableSet(
            new HashSet<>(Arrays.asList("/", "/login")));

    @Override
    public boolean preHandle(
            HttpServletRequest req, HttpServletResponse resp, Object handler) throws Exception {

        if (bypassFilter(req) || hasSession(req)) {
            return true;
        } else {
            resp.sendRedirect(req.getContextPath());
            return false;
        }
    }

    private boolean bypassFilter(HttpServletRequest request) {
        String contextPath = request.getContextPath();
        String path = request.getRequestURI().replaceFirst(contextPath, "");
        return ALLOWED_PATHS.stream().anyMatch(path::equals);
    }

    private boolean hasSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return session != null && session.getAttribute("userId") != null;
    }
}
