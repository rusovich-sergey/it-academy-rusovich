package by.srg.webmvc.service;


import by.srg.model.Role;
import by.srg.model.User;
import by.srg.services.RoleCrud;
import by.srg.services.UserCrud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class AuthService implements UserDetailsService {

    @Autowired
    private UserCrud userCrud;
    @Autowired
    private RoleCrud roleCrud;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {


        User user = null;
        List<Role> roleList = null;

        try {
            roleList = roleCrud.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            user = userCrud.getByLogin(login);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();

        for (Role role : roleList) {
            grantedAuthorities.add(new SimpleGrantedAuthority(role.getName()));
        }

        return new org.springframework.security.core.userdetails.User(
                user.getLogin(), user.getPassword(), grantedAuthorities
        );
    }
}
