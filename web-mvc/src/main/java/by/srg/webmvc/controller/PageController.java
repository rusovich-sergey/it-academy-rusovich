package by.srg.webmvc.controller;

import by.srg.model.Shop;
import by.srg.model.User;
import by.srg.services.AuthorizationService;
import by.srg.services.ShopCrud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class PageController {

    @Autowired
    private AuthorizationService authorizationService;
    @Autowired
    private ShopCrud shopCrud;

    @RequestMapping(path = "/")
    public String home() {
        return "home/homePage";
    }

    @RequestMapping(path = "/login", method = RequestMethod.GET)
    public String loginGet() {
        return "login";
    }

    @RequestMapping(path = "/login", method = RequestMethod.POST)
    public String loginPost(Model model, HttpSession session,
                            @RequestParam String userName,
                            @RequestParam String password) {
        User user;
        List<Shop> allShop = null;

        try {
            allShop = shopCrud.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        user = authorizationService.authorize(userName, password);

        if (user != null) {
            long roleId = user.getRole().getId();
            session.setAttribute("user", user);
            session.setAttribute("allShop", allShop);
            session.setAttribute("userId", user.getId());

            if (user.getIsActive()) {
                if (roleId == 1) {
                    return "forward:/admin";
                } else if (roleId == 2 || roleId == 3) {
                    return "forward:/workspace";
                }
            } else {
                model.addAttribute("errorMessage", "Пользователь не активен");
                session.invalidate();
                return "error/401";
            }

        } else {
            model.addAttribute("errorMessage", "Неправильные имя пользователя или пароль");
            return "error/401";
        }
        return "login";
    }

    @RequestMapping(path = "/logout", method = RequestMethod.GET)
    public String logout(HttpSession session) {
        session.invalidate();
        return "home/homePage";
    }

    @RequestMapping(path = "/workspace")
    public String workspace() {
        return "workspace";
    }

    @RequestMapping(path = "/admin")
    public String adminPanel() {
        return "adminPanel";
    }

}
