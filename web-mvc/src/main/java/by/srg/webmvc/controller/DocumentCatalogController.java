package by.srg.webmvc.controller;

import by.srg.model.Catalog;
import by.srg.model.Document;
import by.srg.model.User;
import by.srg.services.CatalogCrud;
import by.srg.services.DocumentCrud;
import by.srg.services.ShopCrud;
import by.srg.services.StatusCrud;
import by.srg.services.utils.ParserXlsUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

@Controller
public class DocumentCatalogController {

    @Autowired
    private ShopCrud shopCrud;
    @Autowired
    private CatalogCrud catalogCrud;
    @Autowired
    private DocumentCrud documentCrud;
    @Autowired
    private StatusCrud statusCrud;
    @Autowired
    private ParserXlsUtil parserXlsUtil;


    @RequestMapping(path = "/upload", method = RequestMethod.GET)
    public String uploadToDbGet() {
        return "uploadToDb";
    }

    @RequestMapping(path = "/upload", method = RequestMethod.POST)
    public String uploadToDbPost(Model model,
                                 @RequestParam("file") MultipartFile file) {

        InputStream fileInputStream = null;

        try {
            fileInputStream = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String originalFilename = file.getOriginalFilename();

        String nameFile = originalFilename.substring(0, originalFilename.length() - 5);

        parserXlsUtil.readBook(fileInputStream, nameFile);

        model.addAttribute("nameFile", nameFile);


        return "adminPanel";
    }

    @RequestMapping(path = "/list-catalogs/{id}", method = RequestMethod.GET)
    public String documents(Model model, HttpSession session,
                            @PathVariable String id) {

        User user = (User) session.getAttribute("user");

        List<Document> documents = null;
        Catalog catalog = null;

        Long catalogId = Long.valueOf(id);

        try {
            catalog = catalogCrud.read(catalogId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Long statusId = catalog.getStatus().getId();

        //клонирование документа
        if (user.getRole().getId() == 3) {
            if (statusId == 4) {
                try {
                    catalogId = catalogCrud.clone(catalog, user);
                    statusId = 1L;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            documents = documentCrud.getAllByIdCatalog(catalogId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        model.addAttribute("statusId", statusId);
        model.addAttribute("catalogId", catalogId);
        model.addAttribute("documents", documents);

        return "documents";
    }

    @RequestMapping(path = "/list-catalogs", method = RequestMethod.GET)
    public String listCatalogs(Model model) {

        model.addAttribute("listCatalogs", getAllCatalogs());

        return "listCatalogs";
    }

    @RequestMapping(path = "/edit-catalog", method = RequestMethod.GET)
    public String editCatalogGet(Model model) {

        model.addAttribute("listCatalogs", getAllCatalogs());
        return "editCatalogs";
    }

    @RequestMapping(path = "/edit-catalog", method = RequestMethod.POST)
    public String editCatalogPost(HttpSession session,
                                  @RequestParam(required = false) String shop,
                                  @RequestParam(required = false) String[] total,
                                  @RequestParam(required = false) String catalogId,
                                  @RequestParam(required = false) String status,
                                  @RequestParam(required = false) String name) {

        User user = (User) session.getAttribute("user");
        Catalog catalog = null;
        Long longCatalogId = Long.valueOf(catalogId);

        try {
            catalog = new Catalog(
                    longCatalogId,
                    name,
                    statusCrud.read(Long.parseLong(status))
            );

            if (!(shop == null)) {
                catalog.setShop(shopCrud.read(Long.parseLong(shop)));
            }

            catalog.setDateOfChange(new Date());

            catalogCrud.edit(catalog);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!(total == null)) {
            documentCrud.editDocumentList(total, longCatalogId);
        }

        if ((user.getRole().getId() == 2L) || (user.getRole().getId() == 3L)) {
            return "redirect:/list-catalogs";
        } else {
            return "redirect:/edit-catalog";
        }
    }

    @RequestMapping(path = "/delete-catalog", method = RequestMethod.GET)
    public String deleteCatalogGet(Model model) {

        model.addAttribute("listCatalogs", getAllCatalogs());

        return "deleteCatalogs";
    }

    @RequestMapping(path = "/delete-catalog", method = RequestMethod.POST)
    public String deleteCatalogPost(@RequestParam String catalogId) {

        Long id = Long.valueOf(catalogId);

        try {
            catalogCrud.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/delete-catalog";
    }

    private List<Catalog> getAllCatalogs() {
        List<Catalog> catalogs = null;

        try {
            catalogs = catalogCrud.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return catalogs;
    }
}
