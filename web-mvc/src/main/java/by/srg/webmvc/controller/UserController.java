package by.srg.webmvc.controller;

import by.srg.model.Role;
import by.srg.model.User;
import by.srg.services.RoleCrud;
import by.srg.services.UserCrud;
import by.srg.services.utils.PasswordUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserCrud userCrud;
    @Autowired
    private RoleCrud roleCrud;

    @Autowired

    @RequestMapping(path = "/create-users", method = RequestMethod.GET)
    public String createUsersGET() {
        return "createUsers";
    }

    @RequestMapping(path = "/create-users", method = RequestMethod.POST)
    public String createUsersPost(Model model,
                                  @RequestParam String fullName,
                                  @RequestParam String login,
                                  @RequestParam String password,
                                  @RequestParam String roleId,
                                  @RequestParam String isActive) {
        Long id = null;
        Role role = null;

        try {
            role = roleCrud.read(Long.parseLong(roleId));
        } catch (Exception e) {
            e.printStackTrace();
        }
        User user = new User(
                fullName, login, PasswordUtil.getHashString(password), role, Boolean.valueOf(isActive)
        );

        try {
            id = userCrud.create(user);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(id == null) {
            model.addAttribute("message",
                    "Такое имя пользователя уже занято, повторите попытку.");
        }

        model.addAttribute("id", id);
        return "createUsers";
    }

    @RequestMapping(path = "/list-users")
    public String listUsers(Model model) {

        model.addAttribute("listUsers", getAllUser());

        return "listUsers";
    }

    @RequestMapping(path = "/edit-user", method = RequestMethod.GET)
    public String editUsersGet(Model model) {

        model.addAttribute("listUsers", getAllUser());

        return "editUsers";
    }

    @RequestMapping(path = "/edit-user", method = RequestMethod.POST)
    public String editUsersPost(Model model,
                                @RequestParam String userId,
                                @RequestParam String fullName,
                                @RequestParam String login,
                                @RequestParam String password,
                                @RequestParam String roleId,
                                @RequestParam String isActive) {

        Long id = Long.valueOf(userId);
        Role role = null;
        Boolean result = null;

        try {
            role = roleCrud.read(Long.parseLong(roleId));
        } catch (Exception e) {
            e.printStackTrace();
        }

        User user = new User(
                id, fullName, login, PasswordUtil.getHashString(password), role, Boolean.valueOf(isActive)
        );

        try {
            result = userCrud.edit(user);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if(result == false) {
            model.addAttribute("message",
                    "Такое имя пользователя уже занято, повторите попытку.");
        }

        model.addAttribute("listUsers", getAllUser());

        return "editUsers";
    }

    @RequestMapping(path = "/delete-user", method = RequestMethod.GET)
    public String deleteUserGet(Model model) {

        model.addAttribute("listUsers", getAllUser());

        return "deleteUsers";
    }

    @RequestMapping(path = "/delete-user", method = RequestMethod.POST)
    public String deleteUserPost(@RequestParam String userId) {

        Long id = Long.valueOf(userId);

        try {
            userCrud.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "redirect:/delete-user";
    }

    private List<User> getAllUser() {
        List<User> users = null;

        try {
            users = userCrud.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }
}
