<%@ page contentType="text/html;charset=UTF-8" %>

<div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
    <p>Для дальнейшей работы необходимо пройти авторизацию</p>
    <p>Для этого пройдите по следующей ссылке: <a href="<%=request.getContextPath()%>/login">Авторизация</a></p>
</div>
