<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../../css/myPageFooter.css" %>
</style>

<c:if test="${sessionScope.user.role.id == 1}">
    <div>
        <nav>
            <ul class="topmenu">
                <li><a href="<%=request.getContextPath()%>/admin">Главная</a></li>
                <li><a href="#" class="submenu-link">Пользователи</a>
                    <ul class="submenu">
                        <li><a href="<%=request.getContextPath()%>/list-users">Посмотреть всех</a></li>
                        <li><a href="<%=request.getContextPath()%>/create-users">Создать</a></li>
                        <li><a href="<%=request.getContextPath()%>/edit-user">Редактировать</a></li>
                        <li><a href="<%=request.getContextPath()%>/delete-user">Удалить</a></li>
                    </ul>
                <li><a href="#" class="submenu-link">Документы</a>
                    <ul class="submenu">
                        <li><a href="<%=request.getContextPath()%>/list-catalogs">Посмотреть все</a></li>
                        <li><a href="<%=request.getContextPath()%>/upload">Загрузить</a></li>
                        <li><a href="<%=request.getContextPath()%>/edit-catalog">Редактировать</a></li>
                        <li><a href="<%=request.getContextPath()%>/delete-catalog">Удалить</a></li>
                    </ul>
                </li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </nav>
    </div>
</c:if>

<c:if test="${sessionScope.user.role.id == 2 || sessionScope.user.role.id == 3}">
    <div>
        <nav>
            <ul class="topmenu">
                <li><a href="<%=request.getContextPath()%>/workspace">Главная</a></li>
                <li><a href="<%=request.getContextPath()%>/list-catalogs">Прайсы</a>
                <li><a href="#">Контакты</a></li>
            </ul>
        </nav>
    </div>
</c:if>
