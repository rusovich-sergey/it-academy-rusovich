<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<c:if test="${sessionScope.user.role.id == 1}">
    <title>Административная панель</title>
</c:if>
<c:if test="${sessionScope.user.role.id == 2}">
    <title>Рабочая область</title>
</c:if>
