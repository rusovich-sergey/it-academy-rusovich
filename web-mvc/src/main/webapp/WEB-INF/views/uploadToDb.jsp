<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../css/w3.css" %>
    <%@ include file="../css/global.css" %>
    <%@ include file="../css/zatemnenie.css" %>
    <%@ include file="../css/spiral.css" %>
    <%@ include file="../css/loading.css" %>
</style>

<html>

<head>

    <jsp:include page="include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Загрузка документа в базу данных</h2>
        <jsp:include page="include/_logoutMenu.jsp"/>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="include/_menu.jsp"/>
    </div>

</header>

<body>

<div style="padding:5px; color:red;font-style:italic;">
    ${errorMessage}
</div>

<form id="myForm" method="post" action="upload" enctype="multipart/form-data">
    <input type="file" name="file"
           accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/>
    <br/>
    <br/>
    <input onclick="document.location='#zatemnenie'" type="submit" value="Загрузить"/>
</form>

<div id="zatemnenie">
    <div id="okno">
        <div id="cssload-cupcake" class="cssload-box">
            <span class="cssload-letter">L</span>

            <div class="cssload-cupcakeCircle cssload-box">
                <div class="cssload-cupcakeInner cssload-box">
                    <div class="cssload-cupcakeCore cssload-box"></div>
                </div>
            </div>

            <span class="cssload-letter cssload-box">A</span>
            <span class="cssload-letter cssload-box">D</span>
            <span class="cssload-letter cssload-box">I</span>
            <span class="cssload-letter cssload-box">N</span>
            <span class="cssload-letter cssload-box">G</span>
        </div>
    </div>
</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="include/_footer.jsp"/>

</footer>

</html>