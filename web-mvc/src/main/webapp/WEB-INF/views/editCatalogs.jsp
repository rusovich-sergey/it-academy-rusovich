<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../css/global.css" %>
    <%@ include file="../css/w3.css" %>
    select:invalid { color: gray; }
</style>

<html>

<head>

    <jsp:include page="include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Редактирование каталога</h2>
        <jsp:include page="include/_logoutMenu.jsp"/>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="include/_menu.jsp"/>
    </div>

</header>

<body>

<div class="container">

    <form action="edit-catalog" method="post">

        <label for="catalogId">Документ:</label>
        <select id="catalogId" name="catalogId" required>
            <option value="" disabled selected hidden>Выберите документ который хотите редактировать</option>
            <c:forEach var="catalog" items="${listCatalogs}">
                <option value=${catalog.id}><c:out value="${catalog.name}"/></option>
            </c:forEach>
        </select>

        <label for="name">Название</label>
        <input type="text" id="name" name="name" autocomplete="off" placeholder="При необходимости введите новое название">

        <label for="status">Статус</label>
        <select id="status" name="status">
            <option value="-1">При необходимости поменяйте статус документа</option>
            <option value="1">В работе</option>
            <option value="2">Оформлен</option>
            <option value="3">Завершенный</option>
            <option value="4">Новый</option>
            <option value="5">Архивный</option>
        </select>

        <input type="submit" value="Сохранить" id="submit">
    </form>
</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="include/_footer.jsp"/>

</footer>

</html>