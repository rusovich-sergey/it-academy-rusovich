<%@ page contentType="text/html;charset=UTF-8" %>

<style>
    <%@ include file="../css/w3.css" %>
</style>

<html>

<head>

    <title>Авторизация</title>

    <style>
        body {
            display: flex;
            flex-direction: column;
        }
    </style>

</head>

<header class="w3-container  w3-teal">

    <h2>Страница авторизации</h2>

</header>

<body>

<div class="w3-container" style="flex-grow: 1">
    <div class="w3-container w3-half w3-margin-top">
        <div>
            <h3>Пройдите авторизацию -vasya- -12345-</h3>
        </div>
        <div>
            <form method="post" class="w3-container w3-card-4">
                <br/>
                <label>
                    Имя пользователя:
                    <input type="text" name="userName" class="w3-input" style="width:90%" required><br>
                </label>
                <label>
                    Пароль:
                    <input type="password" name="password" class="w3-input" style="width:90%" required><br>
                </label>
                <button type="submit" value="submit" class="w3-button w3-section w3-teal w3-ripple">
                    Войти
                </button>
                <div class="w3-text-red">
                    <%= request.getAttribute("errorMessage") == null ? "" : request.getAttribute("errorMessage")%>
                </div>
            </form>
        </div>
    </div>
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="include/_footer.jsp"/>

</footer>

</html>
