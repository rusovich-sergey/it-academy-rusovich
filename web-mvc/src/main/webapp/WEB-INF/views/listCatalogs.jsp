<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<style>
    <%@ include file="../css/global.css" %>
    <%@ include file="../css/w3.css" %>
    <%@ include file="../css/zatemnenie.css" %>
    <%@ include file="../css/spiral.css" %>
    <%@ include file="../css/loading.css" %>
</style>

<html>

<head>

    <jsp:include page="include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Список документов</h2>
        <jsp:include page="include/_logoutMenu.jsp"/>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="include/_menu.jsp"/>
    </div>

</header>

<body>

<div>
    <table class="w3-table-all w3-centered w3-hoverable">
        <thead>
        <tr>
            <th>Статус</th>
            <th>Наименование</th>
            <th>Магазин</th>
            <th>Пользователь</th>
            <th>Дата загрузки</th>
            <th>Дата редактирования</th>
        </tr>
        </thead>
        <c:forEach var="catalog" items="${listCatalogs}">

            <%--Для продавца--%>
            <c:if test="${sessionScope.user.role.id == 3}">
                <%--Переменная для статуса документа--%>
                <c:set var="statusDoc" value="${catalog.status.id}" scope="request"/>
                <%--Если ((имя пользователя совпадает с именем создателя) и ((статус = work) или (статус = confirmed)))
                или (статус = new)--%>
                <c:if test="${(
                ((sessionScope.user.fullName == catalog.user.fullName) && ((statusDoc == 1) || (statusDoc == 2)))
                ) || (
                (statusDoc == 4)
                )}">
                    <tr onclick="document.location='#zatemnenie', window.location.href='<%=request.getContextPath()%>/list-catalogs/${catalog.id}'">
                        <td>
                            <c:out value="${catalog.status.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.shop.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.user.fullName}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.dateOfCreation}"/>
                        </td>
                        <td>
<%--                            <c:out value="${catalog.dateOfChange}"/>--%>
                            <fmt:formatDate value="${catalog.dateOfChange}" pattern="yyyy-MM-dd HH:mm:ss"/>
                        </td>

                    </tr>
                </c:if>
            </c:if>

            <%--Для менеджера--%>
            <c:if test="${sessionScope.user.role.id == 2}">
                <%--Переменная для статуса документа--%>
                <c:set var="statusDoc" value="${catalog.status.id}" scope="request"/>
                <%--Если (статус = confirmed) или (статус = completed))--%>
                <c:if test="${((statusDoc == 2) || (statusDoc == 3))}">
                    <tr onclick="window.location.href='<%=request.getContextPath()%>/list-catalogs/${catalog.id}'">
                        <td>
                            <c:out value="${catalog.status.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.shop.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.user.fullName}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.dateOfCreation}"/>
                        </td>
                        <td>
<%--                            <c:out value="${catalog.dateOfChange}"/>--%>
                            <fmt:formatDate value="${catalog.dateOfChange}" pattern="yyyy-MM-dd HH:mm:ss"/>
                        </td>
                    </tr>
                </c:if>
            </c:if>

            <%--Для админа--%>
            <c:if test="${sessionScope.user.role.id == 1}">
                <tr onclick="window.location.href='<%=request.getContextPath()%>/list-catalogs/${catalog.id}'">
                    <td>
                        <c:out value="${catalog.status.name}"/>
                    </td>
                    <td>
                        <c:out value="${catalog.name}"/>
                    </td>
                    <td>
                        <c:out value="${catalog.shop.name}"/>
                    </td>
                    <td>
                        <c:out value="${catalog.user.fullName}"/>
                    </td>
                    <td>
                        <c:out value="${catalog.dateOfCreation}"/>
                    </td>
                    <td>
                        <c:out value="${catalog.dateOfChange}"/>
                    </td>
                </tr>
            </c:if>

        </c:forEach>
    </table>
</div>

<div id="zatemnenie">
    <div id="okno">
        <div id="cssload-cupcake" class="cssload-box">
            <span class="cssload-letter">L</span>

            <div class="cssload-cupcakeCircle cssload-box">
                <div class="cssload-cupcakeInner cssload-box">
                    <div class="cssload-cupcakeCore cssload-box"></div>
                </div>
            </div>

            <span class="cssload-letter cssload-box">A</span>
            <span class="cssload-letter cssload-box">D</span>
            <span class="cssload-letter cssload-box">I</span>
            <span class="cssload-letter cssload-box">N</span>
            <span class="cssload-letter cssload-box">G</span>
        </div>
    </div>
</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="include/_footer.jsp"/>

</footer>

</html>