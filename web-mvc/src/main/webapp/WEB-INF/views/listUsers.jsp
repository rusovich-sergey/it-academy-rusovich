<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../css/global.css" %>
    <%@ include file="../css/w3.css" %>
</style>

<html>

<head>

    <jsp:include page="include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Список пользователей</h2>
        <jsp:include page="include/_logoutMenu.jsp"/>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="include/_menu.jsp"/>
    </div>

</header>

<body>

<div>
    <table class="w3-table-all w3-centered w3-hoverable">
        <thead>
        <tr>
            <th>Ид</th>
            <th>ФИО</th>
            <th>Имя пользователя</th>
<%--            <th>Пароль</th>--%>
            <th>Роль</th>
            <th>Флаг активности</th>
        </tr>
        </thead>
        <c:forEach var="user" items="${listUsers}">
            <tr>
                <td>
                    <c:out value="${user.id}"/>
                </td>
                <td>
                    <c:out value="${user.fullName}"/>
                </td>
                <td>
                    <c:out value="${user.login}"/>
                </td>
<%--                <td>--%>
<%--                    <c:out value="${user.password}"/>--%>
<%--                </td>--%>
                <td>
                    <c:out value="${user.role.name}"/>
                </td>
                <td>
                    <c:out value="${user.isActive}"/>
                </td>
            </tr>
        </c:forEach>
    </table>
</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="include/_footer.jsp"/>

</footer>

</html>