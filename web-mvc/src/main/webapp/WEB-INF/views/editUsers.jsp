<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<style>
    <%@ include file="../css/global.css" %>
    <%@ include file="../css/w3.css" %>
    select:invalid { color: gray; }
</style>

<html>

<head>

    <jsp:include page="include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Редактирование пользователя</h2>
        <jsp:include page="include/_logoutMenu.jsp"/>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="include/_menu.jsp"/>
    </div>

</header>

<body>

<div class="container">

    <form method="post" action="edit-user" autocomplete="off">

        <c:if test="${message != null}">
            <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
                <p>${message}</p>
            </div>
        </c:if>

        <label for="userId">Пользователь:</label>
        <select id="userId" name="userId" required>
            <option value="" disabled selected hidden>Выберите пользователя которого хотите редактировать</option>
            <c:forEach var="user" items="${listUsers}">
                <c:set value="${user.login} | ${user.role.name} | ${user.isActive}" var="specifications" scope="page"/>
                <option value=${user.id}><c:out value="${user.fullName} (${specifications})"/></option>
            </c:forEach>
        </select>

        <label for="fullName">ФИО</label>
        <input type="text" id="fullName" name="fullName" placeholder="При необходимости введите новые ФИО">

        <label for="login">Имя пользователя</label>
        <input type="text" id="login" name="login" maxlength="20" placeholder="При необходимости введите новое имя пользователя">

        <label for="password">Пароль</label>
        <input type="text" id="password" name="password" maxlength="20" placeholder="При необходимости введите новый пароль">

        <label for="roleId">Роль</label>
        <select id="roleId" name="roleId" required>
            <option value="-1" selected>При необходимости поменяйте роль</option>
            <option value="2">Менеджер</option>
            <option value="1">Администратор</option>
            <option value="3">Продавец</option>
        </select>

        <label for="isActive">Флаг активности</label>
        <select id="isActive" name="isActive">
            <option value="">При необходимости поменяйте флаг</option>
            <option value="false">False</option>
            <option value="true">True</option>
        </select>

        <input type="submit" value="Сохранить" id="submit">
    </form>

</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="include/_footer.jsp"/>

</footer>

</html>