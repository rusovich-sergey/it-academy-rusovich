<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<style>
    <%@ include file="../css/global.css" %>
    <%@ include file="../css/w3.css" %>
</style>

<html>

<head>

    <jsp:include page="include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Создание пользователя</h2>
        <jsp:include page="include/_logoutMenu.jsp"/>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="include/_menu.jsp"/>
    </div>

</header>

<body>

<div class="container">
    <form method="post" autocomplete="off">

        <c:if test="${message != null}">
            <div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
                <p>${message}</p>
            </div>
        </c:if>

        <label for="fullName">ФИО</label>
        <input type="text" id="fullName" name="fullName"
               placeholder="Введите фамилию имя отчество" required>

        <label for="login">Имя пользователя</label>
        <input type="text" id="login" name="login" maxlength="20"
               placeholder="Имя пользователя должно содержать до 20 символов" required>

        <label for="password">Пароль</label>
        <input type="text" id="password" name="password" maxlength="20"
               placeholder="Пароль должен содержать до 20 символов" required>

        <label for="roleId">Роль</label>
        <select id="roleId" name="roleId">
            <option value="2">Менеджер</option>
            <option value="1">Администратор</option>
            <option value="3">Продавец</option>
        </select>

        <label for="isActive">Флаг активности</label>
        <select id="isActive" name="isActive">
            <option value="false">False</option>
            <option value="true">True</option>
        </select>

        <input type="submit" value="Создать">

    </form>
</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="include/_footer.jsp"/>

</footer>

</html>