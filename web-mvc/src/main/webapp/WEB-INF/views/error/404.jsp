<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../../css/w3.css" %>
</style>

<html>

<head>

    <title>404</title>

    <style>
        body {
            display: flex;
            flex-direction: column;
        }
    </style>

</head>

<header class="w3-container  w3-teal">

    <h2>Ошибка 404!</h2>

</header>

<body>

<div class="w3-panel w3-pale-red w3-leftbar w3-border-red">
    <p>Такой страницы не существует, вернитесь в рабочую область.</p>
    <c:if test="${sessionScope.user.role.id == 2 || sessionScope.user.role.id == 3}">
        <p>Для этого пройдите по следующей ссылке: <a href="<%=request.getContextPath()%>/workspace">Клик</a></p>
    </c:if>
    <c:if test="${sessionScope.user.role.id == 1}">
        <p>Для этого пройдите по следующей ссылке: <a href="<%=request.getContextPath()%>/admin">Клик</a></p>
    </c:if>
</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="../include/_footer.jsp"/>

</footer>

</html>

