<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<style>
    <%@ include file="../../css/w3.css" %>
</style>

<html>

<head>

    <meta http-equiv='refresh' content='10; url=${pageContext.request.contextPath}/login'>

    <title>401</title>

    <style>
        body {
            display: flex;
            flex-direction: column;
        }
    </style>

</head>

<header class="w3-container  w3-teal">

    <h2>Ошибка 401!</h2>
    <br style="background: crimson"><%= request.getAttribute("errorMessage").toString()%>

</header>

<body>

<jsp:include page="../include/_authorization.jsp"/>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="../include/_footer.jsp"/>

</footer>

</html>
