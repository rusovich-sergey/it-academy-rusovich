package by.srg.services;

import by.srg.model.Catalog;
import by.srg.model.User;

import java.util.List;

public interface CatalogCrud extends CRUD<Catalog> {

    List<Catalog> getAll() throws Exception;

    Long clone (Catalog record, User user) throws Exception;
}
