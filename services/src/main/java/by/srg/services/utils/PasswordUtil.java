package by.srg.services.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.security.MessageDigest;

public class PasswordUtil {
    private final static Logger LOGGER = LogManager.getLogger();

    public static String getHashString(String stringToHash) {
        MessageDigest digest = null;
        StringBuilder builder = new StringBuilder();

        try {
            digest = MessageDigest.getInstance("MD5");
        } catch (Exception e) {
            LOGGER.error("NoSuchAlgorithmException. Hash password.");
            e.printStackTrace();
        }

        byte[] strByte = digest.digest(stringToHash.getBytes());
        for (byte b : strByte) {
            builder.append(String.format("%02X ", b));
        }
        return builder.toString().trim();
    }

    public static boolean checkPassword(String password, String stored) {
        String compared;

        if (password != null || password.length() > 0) {
            compared = getHashString(password);
        } else {
            LOGGER.error("Empty password");
            return false;
        }
        return compared.equals(stored);
    }
}
