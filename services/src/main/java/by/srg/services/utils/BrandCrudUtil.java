package by.srg.services.utils;

import by.srg.db.dao.BrandDao;
import by.srg.model.Brand;
import by.srg.services.BrandCrud;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BrandCrudUtil implements BrandCrud {
    private static final Logger LOGGER = LogManager.getLogger();
//    private static BrandCrudUtil brandCrudUtil;

    @Autowired
    private BrandDao brandDao;

//    private static BrandDao brandDao = HibernateBrandDao.getInstance();

    private BrandCrudUtil() {
    }

//    public static BrandCrudUtil getInstance() {
//        if (brandCrudUtil == null) {
//            synchronized (UserCrudUtil.class) {
//                if (brandCrudUtil == null) {
//                    brandCrudUtil = new BrandCrudUtil();
//                }
//            }
//        }
//        return brandCrudUtil;
//    }

    @Override
    public Long create(Brand brand) throws Exception {
        Long result = null;

        try {
            result = brandDao.create(brand);
        } catch (Exception e) {
            LOGGER.info("Ошибка на этапе создания бренда.");
            LOGGER.error(e);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Brand read(Long id) throws Exception {
        if (id == -1) {
            return null;
        } else {
            return brandDao.get(id);
        }
    }

    @Override
    public boolean edit(Brand brand) throws Exception {
        return false;
    }

    @Override
    public boolean delete(Long id) throws Exception {
        return false;
    }
}
