package by.srg.services.utils;

import by.srg.db.dao.ProductDao;
import by.srg.model.Product;
import by.srg.services.ProductCrud;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductCrudUtil implements ProductCrud {
    private static final Logger LOGGER = LogManager.getLogger();
//    private static ProductCrudUtil productCrudUtil;

    @Autowired
    private ProductDao productDao;

//    private static ProductDao productDao = HibernateProductDao.getInstance();

    private ProductCrudUtil() {
    }

//    public static ProductCrudUtil getInstance() {
//        if (productCrudUtil == null) {
//            synchronized (ProductCrudUtil.class) {
//                if (productCrudUtil == null) {
//                    productCrudUtil = new ProductCrudUtil();
//                }
//            }
//        }
//        return productCrudUtil;
//    }

    @Override
    public Long create(Product product) throws Exception {
        Long result = null;
        Long findProductById = productDao.getIdByBarcode(product.getBarcode());
//        if (findProductById == -1) {
        if (findProductById == null) {
            try {
                result = productDao.create(product);
            } catch (Exception e) {
                LOGGER.info("Ошибка на этапе создания продукта.");
                LOGGER.error(e);
                e.printStackTrace();
            }
        } else {
            try {
                product.setId(findProductById);
                productDao.update(product);
                result = findProductById;
            } catch (Exception e) {
                LOGGER.info("Ошибка на этапе обновления продукта.");
                LOGGER.error(e);
                e.printStackTrace();
            }
        }

        return result;
    }

    @Override
    public Product read(Long id) throws Exception {
        if (id == -1) {
            return null;
        } else {
            return productDao.get(id);
        }
    }

    @Override
    public boolean edit(Product product) throws Exception {
        return false;
    }

    @Override
    public boolean delete(Long id) throws Exception {
        return false;
    }
}
