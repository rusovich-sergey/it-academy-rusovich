package by.srg.services.utils;

import by.srg.db.dao.StatusDao;
import by.srg.model.Status;
import by.srg.services.StatusCrud;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StatusCrudUtil implements StatusCrud {
    private static final Logger LOGGER = LogManager.getLogger();
//    private static StatusCrudUtil statusCrudUtil;

    @Autowired
    private StatusDao statusDao;

//    private static StatusDao statusDao = HibernateStatusDao.getInstance();

    private StatusCrudUtil() {
    }

//    public static StatusCrudUtil getInstance() {
//        if (statusCrudUtil == null) {
//            synchronized (StatusCrudUtil.class) {
//                if (statusCrudUtil == null) {
//                    statusCrudUtil = new StatusCrudUtil();
//                }
//            }
//        }
//        return statusCrudUtil;
//    }

    @Override
    public Long create(Status status) throws Exception {
        return null;
    }

    @Override
    public Status read(Long id) throws Exception {
        if (id == -1) {
            return null;
        } else {
            return statusDao.get(id);
        }
    }

    @Override
    public boolean edit(Status status) throws Exception {
        return false;
    }

    @Override
    public boolean delete(Long id) throws Exception {
        return false;
    }
}
