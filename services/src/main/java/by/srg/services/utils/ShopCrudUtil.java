package by.srg.services.utils;

import by.srg.db.dao.ShopDao;
import by.srg.model.Shop;
import by.srg.services.ShopCrud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShopCrudUtil implements ShopCrud {
//    private static ShopCrudUtil shopCrudUtil;

    @Autowired
    private ShopDao shopDao;

//    private static ShopDao shopDao = HibernateShopDao.getInstance();
//    private static ShopDao shopDao = context.getBean("springShopDao", SpringShopDao.class);

    private ShopCrudUtil() {
    }

//    public static ShopCrudUtil getInstance() {
//        if (shopCrudUtil == null) {
//            synchronized (ShopCrudUtil.class) {
//                if (shopCrudUtil == null) {
//                    shopCrudUtil = new ShopCrudUtil();
//                }
//            }
//        }
//        return shopCrudUtil;
//    }

    @Override
    public Long create(Shop shop) throws Exception {
        return null;
    }

    @Override
    public Shop read(Long id) throws Exception {
        if (id == -1) {
            return null;
        } else {
            return shopDao.get(id);
        }
    }

    @Override
    public boolean edit(Shop shop) throws Exception {
        return false;
    }

    @Override
    public boolean delete(Long id) throws Exception {
        return false;
    }

    @Override
    public List<Shop> getAll() throws Exception {
        List<Shop> result = null;

        try {
            result = shopDao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
