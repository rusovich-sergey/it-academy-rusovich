package by.srg.services.utils;

import by.srg.db.dao.RoleDao;
import by.srg.model.Role;
import by.srg.services.RoleCrud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleCrudUtil implements RoleCrud {
//    private static RoleCrudUtil roleCrudUtil;

    @Autowired
    private RoleDao roleDao;

//    private static RoleDao roleDao = JdbcRoleDao.getInstance();
//    private static RoleDao roleDao = HibernateRoleDao.getInstance();

    private RoleCrudUtil() {
    }

//    public static RoleCrudUtil getInstance() {
//        if (roleCrudUtil == null) {
//            synchronized (RoleCrudUtil.class) {
//                if (roleCrudUtil == null) {
//                    roleCrudUtil = new RoleCrudUtil();
//                }
//            }
//        }
//        return roleCrudUtil;
//    }

    @Override
    public Long create(Role role) throws Exception {
        return null;
    }

    @Override
    public Role read(Long id) throws Exception {
        if (id == -1) {
            return null;
        } else {
            return roleDao.get(id);
        }
    }

    @Override
    public boolean edit(Role role) throws Exception {
        return false;
    }

    @Override
    public boolean delete(Long id) throws Exception {
        return false;
    }

    @Override
    public List<Role> getAll() throws Exception {
        return roleDao.getAll();
    }
}
