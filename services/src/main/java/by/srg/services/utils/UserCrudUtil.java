package by.srg.services.utils;

import by.srg.db.dao.UserDao;
import by.srg.model.User;
import by.srg.services.UserCrud;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserCrudUtil implements UserCrud {
    private static final Logger LOGGER = LogManager.getLogger();
//    private static UserCrudUtil crudUserUtil;

    @Autowired
    private UserDao userDao;

//    private static UserDao userDao = JdbcUserDao.getInstance();
//    private static UserDao userDao = HibernateUserDao.getInstance();

    private UserCrudUtil() {
    }

//    public static UserCrudUtil getInstance() {
//        if (crudUserUtil == null) {
//            synchronized (UserCrudUtil.class) {
//                if (crudUserUtil == null) {
//                    crudUserUtil = new UserCrudUtil();
//                }
//            }
//        }
//        return crudUserUtil;
//    }

    @Override
    public Long create(User user) {
        Long result = null;

        try {
            result = userDao.create(user);
        } catch (Exception e) {
            LOGGER.info("Ошибка на этапе создания пользователя.");
            LOGGER.error(e);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public User read(Long id) throws Exception {
        if (id == -1) {
            return null;
        } else {
            return userDao.get(id);
        }
    }

    @Override
    public boolean edit(User newUser) throws Exception {
        boolean result = false;

        //Пользователь которого будем менять.
        User oldUser = userDao.get(newUser.getId());

        //Если пришли пустые параметры, заполняем их старыми данными
        if (newUser.getFullName().equals("")) {
            newUser.setFullName(oldUser.getFullName());
        }
        if (newUser.getLogin().equals("")) {
            newUser.setLogin(oldUser.getLogin());
        }

        if (newUser.getPassword().equals("D4 1D 8C D9 8F 00 B2 04 E9 80 09 98 EC F8 42 7E")) {
            newUser.setPassword(oldUser.getPassword());
        }
        if (newUser.getRole() == null) {
            newUser.setRole(oldUser.getRole());
        }
        if (newUser.getIsActive() == null) {
            newUser.setIsActive(oldUser.getIsActive());
        }

        try {
            userDao.update(newUser);
            result = true;
        } catch (Exception e) {
            LOGGER.info("Ошибка на этапе редактирования пользователя.");
            LOGGER.error(e);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean delete(Long id) {
        try {
            userDao.delete(id);
        } catch (Exception e) {
            LOGGER.info("Ошибка на этапе удаления пользователя.");
            LOGGER.error(e);
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<User> getAll() {

        List<User> result = null;

        try {
            result = userDao.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public User getByLogin(String login) throws Exception {
        return userDao.getByLogin(login);
    }
}
