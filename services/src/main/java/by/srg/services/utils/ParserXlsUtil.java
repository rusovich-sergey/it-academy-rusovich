package by.srg.services.utils;

import by.srg.db.dao.BrandDao;
import by.srg.model.Brand;
import by.srg.model.Catalog;
import by.srg.model.Document;
import by.srg.model.Product;
import by.srg.services.*;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

@Service
public class ParserXlsUtil {

    //номера колонок таблицы соответствующие колонкам БД
    private static final int ARTICLE_COLUMN = 1;
    private static final int BARCODE_COLUMN = 2;
    private static final int NAME_COLUMN = 3;
    private static final int PRICE_COLUMN = 6;
    private static final int BALANCE_COLUMN = 7;

    @Autowired
    private CatalogCrud catalogCrud;
    @Autowired
    private StatusCrud statusCrud;
    @Autowired
    private BrandCrud brandCrud;
    @Autowired
    private ProductCrud productCrud;
    @Autowired
    private DocumentCrud documentCrud;
    @Autowired
    private BrandDao brandDao;

//    private CatalogCrud catalogCrud = CatalogCrudUtil.getInstance();
//    private StatusCrud statusCrud = StatusCrudUtil.getInstance();
//    private BrandCrud brandCrud = BrandCrudUtil.getInstance();
//    private ProductCrud productCrud = ProductCrudUtil.getInstance();
//    private DocumentCrud documentCrud = DocumentCrudUtil.getInstance();
//    private BrandDao brandDao = HibernateBrandDao.getInstance();

//    private static ParserXlsUtil parserXlsUtil;

    private ParserXlsUtil() {
    }

//    public static ParserXlsUtil getInstance() {
//        if (parserXlsUtil == null) {
//            synchronized (ParserXlsUtil.class) {
//                if (parserXlsUtil == null) {
//                    parserXlsUtil = new ParserXlsUtil();
//                }
//            }
//        }
//        return parserXlsUtil;
//    }

//    public String readBook(Part part) {
    public String readBook(InputStream file, String nameFile) {

//        String nameFile = extractFileName(part);
        String brandFromFileName = null;

        Catalog newCatalog = null;
        Brand newBrand = null;
        Product newProduct = null;
        Document newDocument = null;

        XSSFCell barcodeCell = null;
        XSSFCell priceCell = null;
        XSSFRow row = null;

        Float price = null;
        int lineCount = 1;

        //из имени файла берем название бренда
        brandFromFileName = nameFile.substring(2, nameFile.length() - 17);

        //записываем новый каталог в БД
        try {
            newCatalog = creatNewCatalog(nameFile);
            catalogCrud.create(newCatalog);
        } catch (Exception e) {
            e.printStackTrace();
        }

        //создаем бренд если такого нету
        try {
            Long brandId = brandDao.getIdByName(brandFromFileName);
//            if (brandId == -1L) {
            if (brandId == null) {
                newBrand = new Brand(brandFromFileName);
                brandCrud.create(newBrand);
            } else {
                newBrand = brandDao.get(brandId);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

//        try (XSSFWorkbook workbook = new XSSFWorkbook(part.getInputStream())) { //получаем книгу
        try (XSSFWorkbook workbook = new XSSFWorkbook(file)) { //получаем книгу
            XSSFSheet sheet = workbook.getSheetAt(0);  // получаем лист
            Iterator<Row> rowIterator = sheet.iterator();  //итератор по всем строкам листа

            //пропускаем первые шесть строк листа (шапка)
            for (int i = 0; i < 6; i++) {
                if (rowIterator.hasNext()) {
                    rowIterator.next();
                }
            }

            while (rowIterator.hasNext()) {

                row = (XSSFRow) rowIterator.next();
                barcodeCell = row.getCell(BARCODE_COLUMN);
                priceCell = row.getCell(PRICE_COLUMN);

                //Пропускаем строки в которых нет штрихкода
                if (barcodeCell == null || barcodeCell.getStringCellValue().equals("")) {
                    continue;
                }

                //если есть штрихкод, сохраняем цену из строки
                if (priceCell.getCellType() == CellType.BLANK) {
                    price = 0F;
                } else {
                    price = (float) priceCell.getNumericCellValue();
                }

                //создаем продукты
                newProduct = createNewProduct(row, newBrand);

                //создаем строки документа
                try {
                    newDocument = new Document(lineCount, 0, price, newCatalog, newProduct);
                    documentCrud.create(newDocument);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                lineCount++;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return nameFile;
    }

    private Catalog creatNewCatalog(String name) {
        Date date = null;
        Catalog result = null;

        //из файла ! OYSTER Остатки 12.05.20 берем 12.05.20
//        String substring = name.substring(name.length() - 8);
//
//        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yy");
//        try {
//            date = format.parse(substring);
//        } catch (ParseException e) {
//            e.printStackTrace();
//        }

        try {
            result = new Catalog(
                    name,
//                    date,
                    new Date(),
                    statusCrud.read(4L));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }

    private Product createNewProduct(Row row, Brand brand) {
        Product result = null;

        String barcode = row.getCell(BARCODE_COLUMN).getStringCellValue();
        String article = row.getCell(ARTICLE_COLUMN).getStringCellValue();
        String name = row.getCell(NAME_COLUMN).getStringCellValue();
        Integer balance = (int) row.getCell(BALANCE_COLUMN).getNumericCellValue();

        try {
            result = new Product(barcode, article, name, brand, balance);
            productCrud.create(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }
}
