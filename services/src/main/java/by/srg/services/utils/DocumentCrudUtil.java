package by.srg.services.utils;

import by.srg.db.dao.DocumentDao;
import by.srg.model.Document;
import by.srg.services.DocumentCrud;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentCrudUtil implements DocumentCrud {
    private static final Logger LOGGER = LogManager.getLogger();
//    private static DocumentCrudUtil documentCrudUtil;

    @Autowired
    private DocumentDao documentDao;

//    private static DocumentDao documentDao = HibernateDocumentDao.getInstance();

    private DocumentCrudUtil() {
    }

//    public static DocumentCrudUtil getInstance() {
//        if (documentCrudUtil == null) {
//            synchronized (UserCrudUtil.class) {
//                if (documentCrudUtil == null) {
//                    documentCrudUtil = new DocumentCrudUtil();
//                }
//            }
//        }
//        return documentCrudUtil;
//    }

    @Override
    public List<Document> getAllByIdCatalog(Long id) {
        List<Document> result = null;

        try {
            result = documentDao.getAllByIdCatalog(id);
        } catch (Exception e) {
            LOGGER.info("Ошибка на этапе получения документов по id каталога.");
            LOGGER.error(e);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Long create(Document document) throws Exception {
        Long result = null;

        try {
            result = documentDao.create(document);
        } catch (Exception e) {
            LOGGER.info("Ошибка на этапе создания документа.");
            LOGGER.error(e);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Document read(Long id) throws Exception {
        return null;
    }

    @Override
    public boolean edit(Document document) throws Exception {
        boolean result = false;

        try {
            documentDao.update(document);
            result = true;
        } catch (Exception e) {
            LOGGER.info("Ошибка на этапе изменения кол-ва.");
            LOGGER.error(e);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean delete(Long id) throws Exception {
        return false;
    }

    @Override
    public void editDocumentList(String[] records, Long catalogId) {

        List<Document> documentList = getAllByIdCatalog(catalogId);

        for (int i = 0; i < records.length; i++) {
            if (!(records[i].equals(""))) {
                int x = Integer.parseInt(records[i]);
                Document document = documentList.get(i);
                document.setTotal(x);
                try {
                    edit(document);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

    }
}
