package by.srg.services.utils;

import by.srg.db.dao.CatalogDao;
import by.srg.db.dao.StatusDao;
import by.srg.model.Catalog;
import by.srg.model.Document;
import by.srg.model.User;
import by.srg.services.CatalogCrud;
import by.srg.services.DocumentCrud;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class CatalogCrudUtil implements CatalogCrud {
    private static final Logger LOGGER = LogManager.getLogger();
//    private static CatalogCrudUtil catalogCrudUtil;

    @Autowired
    private CatalogDao catalogDao;
    @Autowired
    private StatusDao statusDao;
    @Autowired
    private DocumentCrud documentCrud;

//    private static CatalogDao catalogDao = HibernateCatalogDao.getInstance();

    private CatalogCrudUtil() {
    }

//    public static CatalogCrudUtil getInstance() {
//        if (catalogCrudUtil == null) {
//            synchronized (UserCrudUtil.class) {
//                if (catalogCrudUtil == null) {
//                    catalogCrudUtil = new CatalogCrudUtil();
//                }
//            }
//        }
//        return catalogCrudUtil;
//    }

    @Override
    public Long create(Catalog catalog) throws Exception {
        Long result = null;

        try {
            result = catalogDao.create(catalog);
        } catch (Exception e) {
            LOGGER.info("Ошибка на этапе создания каталога.");
            LOGGER.error(e);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Catalog read(Long id) throws Exception {
        if (id == -1) {
            return null;
        } else {
            return catalogDao.get(id);
        }
    }

    @Override
    public boolean edit(Catalog newCatalog) throws Exception {
        boolean result = false;

        //Каталог который будем менять.
        Catalog oldCatalog = catalogDao.get(newCatalog.getId());

        //Если пришли пустые параметры, заполняем их старыми данными
        if (newCatalog.getName() == null || newCatalog.getName().equals("")) {
            newCatalog.setName(oldCatalog.getName());
        }
        if (newCatalog.getStatus() == null) {
            newCatalog.setStatus(oldCatalog.getStatus());
        }
        if(newCatalog.getShop() == null) {
            newCatalog.setShop(oldCatalog.getShop());
        }
        if(newCatalog.getDateOfChange() == null) {
            newCatalog.setDateOfChange(oldCatalog.getDateOfChange());
        }
        if(newCatalog.getDateOfCreation() == null) {
            newCatalog.setDateOfCreation(oldCatalog.getDateOfCreation());
        }
//        newCatalog.setDateOfChange(oldCatalog.getDateOfChange());
//        newCatalog.setDateOfCreation(oldCatalog.getDateOfCreation());
        newCatalog.setUser(oldCatalog.getUser());
        try {
            catalogDao.update(newCatalog);
            result = true;
        } catch (Exception e) {
            LOGGER.info("Ошибка на этапе редактирования документа.");
            LOGGER.error(e);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public boolean delete(Long id) throws Exception {
        try {
            catalogDao.delete(id);
        } catch (Exception e) {
            LOGGER.info("Ошибка на этапе удаления пользователя.");
            LOGGER.error(e);
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public List<Catalog> getAll() throws Exception {
        List<Catalog> result = null;

        try {
            result = catalogDao.getAll();
        } catch (Exception e) {
            LOGGER.info("Ошибка на этапе получения всех каталогов.");
            LOGGER.error(e);
            e.printStackTrace();
        }
        return result;
    }

    @Override
    public Long clone(Catalog record, User user) throws Exception {
        Long result = null;
        Catalog newCatalog = null;

        newCatalog = record.clone();

        newCatalog.setId(null);
        newCatalog.setStatus(statusDao.get(1L));
        newCatalog.setUser(user);
        newCatalog.setDateOfChange(new Date());

        result = create(newCatalog);

        List<Document> documentList = record.getDocumentList();

        for (Document document : documentList) {
            Document newDocument;

            newDocument = document.clone();

            newDocument.setId(null);
            newDocument.setCatalog(newCatalog);

            documentCrud.create(newDocument);
        }
        return result;
    }
}
