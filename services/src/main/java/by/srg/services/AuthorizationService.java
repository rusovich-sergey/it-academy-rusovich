package by.srg.services;

import by.srg.model.User;

public interface AuthorizationService {

    User authorize(String login, String password);
}
