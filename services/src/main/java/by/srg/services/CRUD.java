package by.srg.services;

public interface CRUD<T> {

    Long create(T model) throws Exception;

    T read(Long id) throws Exception;

    boolean edit(T model) throws Exception;

    boolean delete(Long id) throws Exception;

}
