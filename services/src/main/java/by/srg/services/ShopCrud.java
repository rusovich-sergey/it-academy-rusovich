package by.srg.services;

import by.srg.model.Shop;

import java.util.List;

public interface ShopCrud extends CRUD<Shop> {

    List<Shop> getAll() throws Exception;
}
