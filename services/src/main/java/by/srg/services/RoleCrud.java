package by.srg.services;

import by.srg.model.Role;

import java.util.List;

public interface RoleCrud extends CRUD<Role> {

    List<Role> getAll() throws Exception;
}
