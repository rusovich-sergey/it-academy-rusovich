package by.srg.services;

import by.srg.model.User;

import java.util.List;

public interface UserCrud extends CRUD<User> {

    List<User> getAll() throws Exception;

    User getByLogin(String login) throws Exception;
}
