package by.srg.services;

import by.srg.model.Document;

import java.util.List;

public interface DocumentCrud extends CRUD<Document> {

    List<Document> getAllByIdCatalog(Long id);

    void editDocumentList (String[] records, Long catalogId);
}
