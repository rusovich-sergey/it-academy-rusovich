package by.srg.services.impl;

import by.srg.model.User;
import by.srg.services.AuthorizationService;
import by.srg.services.UserCrud;
import by.srg.services.utils.PasswordUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorizationServiceImpl implements AuthorizationService {
    private static final Logger LOGGER = LogManager.getLogger();
//    private static AuthorizationService authorizationService = null;

    @Autowired
    private UserCrud userCrud;

//    private UserCrud userCrud = UserCrudUtil.getInstance();

    private AuthorizationServiceImpl() {
    }

//    public static AuthorizationService getInstance() {
//        if (authorizationService == null) {
//            synchronized (AuthorizationServiceImpl.class) {
//                if (authorizationService == null) {
//                    authorizationService = new AuthorizationServiceImpl();
//                    return authorizationService;
//                }
//            }
//        }
//        return authorizationService;
//    }

    @Override
    public User authorize(String login, String password) {
        List<User> users = null;

        try {
            users = userCrud.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (users != null) {
            for (User user : users) {
                if (login.equals(user.getLogin()) &&
                        PasswordUtil.checkPassword(password, user.getPassword())) {
                    return user;
                }
            }
        }

//        if (users != null) {
//            for (User user : users) {
//                if (login.equals(user.getLogin()) && password.equals(user.getPassword())) {
//                    return user;
//                }
//            }
//        }
        return null;
    }
}
