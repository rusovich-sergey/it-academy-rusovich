package by.srg.services.util;

import by.srg.db.connection.HibernateProvider;
import by.srg.model.*;
import by.srg.services.utils.PasswordUtil;
import org.junit.Test;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.util.Date;

public class CreatTestDBHibernate {

//        @Test
    public void testConnection() {
        EntityManager entityManager = HibernateProvider.getSessionFactory().createEntityManager();
        Role role1 = new Role("Администратор");
        Role role2 = new Role("Менеджер");
        Role role3 = new Role("Продавец");
        User user1 = new User("Василий Васильевич Василевич", "vasya", PasswordUtil.getHashString("12345"), role1, true);
        User user2 = new User("Надежда Вячеславовна Явкова", "nadejda", PasswordUtil.getHashString("123456"), role2, false);
        Shop shop1 = new Shop("Минский магазин", "г. Минск, ул Минская, д.55, пав. 5");
        Status status1 = new Status("В работе");
        Status status2 = new Status("Оформляется");
        Status status3 = new Status("Завершенный");
        Status status4 = new Status("Новый");
        Status status5 = new Status("Архивный");
        Brand brand1 = new Brand("KAPOUS");
        Brand brand2 = new Brand("NEXXT");
        Brand brand3 = new Brand("BRELIL");
        Product product1 = new Product("9999999999999999", "АЗ-999", "Шампунь", brand1, 110);
        Product product2 = new Product("5987589683256", "2035", "Расческа", brand1, 10);
        Product product3 = new Product("4586985239657", "A-1/55 K", "Фен", brand1, 3);
        Product product4 = new Product("a6434593059243d", "634 КЗ", "Лак", brand2, 1);
        Catalog catalog1 = new Catalog("Заказ Kapous", new Date(202020), Date.from(Instant.now()), status1, shop1, user1);
        Catalog catalog2 = new Catalog("Заказ Nexxt", new Date(2020), Date.from(Instant.now()), status4, null, null);
        Document document1 = new Document(1, 12, 20.18F, catalog1, product1);
        Document document2 = new Document(2, 1, 155.1F, catalog1, product2);
        Document document3 = new Document(3, 13, 29.90F, catalog1, product3);
        Document document4 = new Document(1, 1, 10.13F, catalog2, product4);
        entityManager.getTransaction().begin();
        entityManager.persist(role1);
        entityManager.persist(role2);
        entityManager.persist(role3);
        entityManager.persist(user1);
        entityManager.persist(user2);
        entityManager.persist(shop1);
        entityManager.persist(status1);
        entityManager.persist(status2);
        entityManager.persist(status3);
        entityManager.persist(status3);
        entityManager.persist(status4);
        entityManager.persist(status5);
        entityManager.persist(brand1);
        entityManager.persist(brand2);
        entityManager.persist(brand3);
        entityManager.persist(product1);
        entityManager.persist(product2);
        entityManager.persist(product3);
        entityManager.persist(product4);
        entityManager.persist(catalog1);
        entityManager.persist(catalog2);
        entityManager.persist(document1);
        entityManager.persist(document2);
        entityManager.persist(document3);
        entityManager.persist(document4);

        entityManager.getTransaction().commit();
        HibernateProvider.close();
    }
}
