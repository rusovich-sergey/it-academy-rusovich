package by.srg.services.util;

import by.srg.db.repository.*;
import by.srg.model.*;
import by.srg.services.utils.PasswordUtil;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Instant;
import java.util.Arrays;
import java.util.Date;

//@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath*:db-spring-context.xml"})
public class CreatTestDBSpring {

    @Autowired
    BrandRepository brandRepository;
    @Autowired
    CatalogRepository catalogRepository;
    @Autowired
    DocumentRepository documentRepository;
    @Autowired
    ProductRepository productRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    ShopRepository shopRepository;
    @Autowired
    StatusRepository statusRepository;
    @Autowired
    UserRepository userRepository;

    public void testConnection() {

        Role role1 = new Role("Администратор");
        Role role2 = new Role("Менеджер");
        Role role3 = new Role("Продавец");
        User user1 = new User("Василий Васильевич Василевич", "vasya", PasswordUtil.getHashString("12345"), role1, true);
        User user2 = new User("Надежда Вячеславовна Явкова", "nadejda", PasswordUtil.getHashString("123456"), role2, false);
        Shop shop1 = new Shop("Минский магазин", "г. Минск, ул Минская, д.55, пав. 5");
        Status status1 = new Status("В работе");
        Status status2 = new Status("Оформляется");
        Status status3 = new Status("Завершенный");
        Status status4 = new Status("Новый");
        Status status5 = new Status("Архивный");
        Brand brand1 = new Brand("KAPOUS");
        Brand brand2 = new Brand("NEXXT");
        Brand brand3 = new Brand("BRELIL");
        Product product1 = new Product("9999999999999999", "АЗ-999", "Шампунь", brand1, 110);
        Product product2 = new Product("5987589683256", "2035", "Расческа", brand1, 10);
        Product product3 = new Product("4586985239657", "A-1/55 K", "Фен", brand1, 3);
        Product product4 = new Product("a6434593059243d", "634 КЗ", "Лак", brand2, 1);
        Catalog catalog1 = new Catalog("Заказ Kapous", new Date(202020), Date.from(Instant.now()), status1, shop1, user1);
        Catalog catalog2 = new Catalog("Заказ Nexxt", new Date(2020), Date.from(Instant.now()), status4, null, null);
        Document document1 = new Document(1, 12, 20.18F, catalog1, product1);
        Document document2 = new Document(2, 1, 155.1F, catalog1, product2);
        Document document3 = new Document(3, 13, 29.90F, catalog1, product3);
        Document document4 = new Document(1, 1, 10.13F, catalog2, product4);

        roleRepository.saveAll(Arrays.asList(role1, role2, role3));
        userRepository.saveAll(Arrays.asList(user1, user2));
        shopRepository.save(shop1);
        statusRepository.saveAll(Arrays.asList(status1, status2, status3, status4, status5));
        brandRepository.saveAll(Arrays.asList(brand1, brand2, brand3));
        productRepository.saveAll(Arrays.asList(product1, product2, product3, product4));
        catalogRepository.saveAll(Arrays.asList(catalog1, catalog2));
        documentRepository.saveAll(Arrays.asList(document1, document2, document3, document4));
    }
}
