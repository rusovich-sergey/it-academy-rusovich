package by.srg.services.util;

import by.srg.model.Product;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import javax.servlet.http.Part;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Runner {

    private static final int articleColumn = 1;
    private static final int barcodeColumn = 2;
    private static final int nameColumn = 3;
    private static final int priceColumn = 6;
    private static final int balanceColumn = 7;


    public static void main(String[] args) {
        List<Product> productList = new ArrayList<>();

        //        try (XSSFWorkbook myExcelBook = new XSSFWorkbook(part.getInputStream())) { //получаем книгу
//            XSSFSheet myExcelSheet = myExcelBook.getSheetAt(0);  // получаем лист
//            XSSFRow row = myExcelSheet.getRow(2);  // получаем строку
//
//            if (row.getCell(1).getCellType() == CellType.STRING) {
//                String name = row.getCell(1).getStringCellValue();
//                System.out.println(name);
//            }
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return null;

        FileInputStream inputStream = null;
        try {
            inputStream = new FileInputStream(new File("d:/! OYSTER Остатки 12.05.20.xlsx"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try (XSSFWorkbook workbook = new XSSFWorkbook(inputStream)) { //получаем книгу
            XSSFSheet sheet = workbook.getSheetAt(0);  // получаем лист
            XSSFRow myExcelRow = sheet.getRow(2);  // получаем строку
//            Iterator<Cell> iteratorCell = myExcelSheet.getRow(2).iterator();// получаем строку

            Iterator<Row> rowIterator = sheet.iterator();


            for (int i = 0; i < 6; i++) {
                if (rowIterator.hasNext()) {
                    rowIterator.next();
                }
            }

            while (rowIterator.hasNext()) {

                XSSFRow row = (XSSFRow) rowIterator.next();

                XSSFCell articleCell = row.getCell(articleColumn);
                XSSFCell barcodeCell = row.getCell(barcodeColumn);
                XSSFCell nameCell = row.getCell(nameColumn);
                XSSFCell priceCell = row.getCell(priceColumn);
                XSSFCell balanceCell = row.getCell(balanceColumn);

                if (barcodeCell == null) {
                    continue;
                }

                if (!barcodeCell.getStringCellValue().equals("")) {
                    System.out.println(
                            articleCell + "  -----  " + articleCell.getCellType() + "  -----  " +
                                    barcodeCell + "  -----  " + barcodeCell.getCellType() + "  -----  " +
                                    nameCell + "  -----  " + nameCell.getCellType() + "  -----  " +
                                    priceCell + "  -----  " + priceCell.getCellType() + "  -----  " +
                                    balanceCell + "  -----  " + balanceCell.getCellType() + "  -----  "
                    );
                    System.out.println("===========================================================" +
                            "======================================================================" +
                            "======================================================================");

                }

//                Row row = rowIterator.next();
//                System.out.println(row.getCell(2));


//                Row row = rowIterator.next();
//                Iterator<Cell> cellIterator = row.cellIterator();

//                while (cellIterator.hasNext()) {
//                    Cell cell = cellIterator.next();
//
//                    System.out.println(cell);
//                    System.out.println("----------------------");
//
//                }
            }

//            if (row.getCell(0).getCellType() == CellType.STRING) {
//                String name = row.getCell(0).getStringCellValue();
//                System.out.println(name);
//            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        //form-data; name="file"; filename="*.xlsx"

        for (String s : items) {
            if (s.trim().startsWith("filename")) {

                String clientFileName = s.substring(s.indexOf("=") + 2, s.length() - 1);

                return clientFileName;
            }
        }
        return null;
    }
}

