package by.srg.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "shops")
@Component
public class Shop extends Model {

    @Column(name = "name", length = 45, nullable = false, unique = true)
    private String name;

    @Column(name = "addres", columnDefinition = "VARCHAR(100) DEFAULT '-address-'")
    private String address;

    @OneToMany(mappedBy = "shop", fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private List<Catalog> catalogList = new ArrayList<>();

    public Shop() {
    }

    public Shop(Long id, String name, String address) {
        super(id);
        this.name = name;
        this.address = address;
    }

    public Shop(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
