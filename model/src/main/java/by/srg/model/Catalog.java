package by.srg.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "catalog")
@Component
public class Catalog extends Model implements Cloneable {

    @Column(name = "name", length = 45, nullable = false)
    private String name;

    @Column(name = "date_of_creation", nullable = false)
    @Temporal(TemporalType.DATE)
    private Date dateOfCreation;

    @Column(name = "date_of_change")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateOfChange;

    @ManyToOne
    @JoinColumn(name = "status_id", nullable = false)
    private Status status;

    @ManyToOne
    @JoinColumn(name = "shop_id")
    private Shop shop;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "catalog", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Document> documentList = new ArrayList<>();

    public Catalog() {
    }

    public Catalog(Long id, String name, Date dateOfCreation, Date dateOfChange, Status status, Shop shop, User user) {
        super(id);
        this.name = name;
        this.dateOfCreation = dateOfCreation;
        this.dateOfChange = dateOfChange;
        this.status = status;
        this.shop = shop;
        this.user = user;
    }

    public Catalog(String name, Date dateOfCreation, Date dateOfChange, Status status, Shop shop, User user) {
        this.name = name;
        this.dateOfCreation = dateOfCreation;
        this.dateOfChange = dateOfChange;
        this.status = status;
        this.shop = shop;
        this.user = user;
    }

    public Catalog(String name, Date dateOfCreation, Status status) {
        this.name = name;
        this.dateOfCreation = dateOfCreation;
        this.status = status;
    }

    public Catalog(Long id, String name, Status status) {
        super(id);
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getDateOfCreation() {
        return dateOfCreation;
    }

    public void setDateOfCreation(Date dateOfCreation) {
        this.dateOfCreation = dateOfCreation;
    }

    public Date getDateOfChange() {
        return dateOfChange;
    }

    public void setDateOfChange(Date dateOfChange) {
        this.dateOfChange = dateOfChange;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Document> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<Document> documentList) {
        this.documentList = documentList;
    }

    public void addToDocumentList(Document document) {
        this.documentList.add(document);
    }

    @Override
    public Catalog clone() throws CloneNotSupportedException {
        return (Catalog) super.clone();
    }
}
