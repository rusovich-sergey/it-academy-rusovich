package by.srg.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "status")
@Component
public class Status extends Model {

    @Column(name = "name", nullable = false, unique = true,
            columnDefinition = "ENUM('Новый','В работе','Оформляется','Завершенный','Архивный')")
    private String name;

    @OneToMany(mappedBy = "status", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Catalog> catalogList = new ArrayList<>();

    public Status() {
    }

    public Status(Long id, String name) {
        super(id);
        this.name = name;
    }

    public Status(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Catalog> getCatalogList() {
        return catalogList;
    }

    public void setCatalogList(List<Catalog> catalogList) {
        this.catalogList = catalogList;
    }

    public void addToCatalogList(Catalog catalog) {
        this.catalogList.add(catalog);
    }
}
