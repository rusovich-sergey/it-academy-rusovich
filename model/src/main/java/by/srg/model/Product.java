package by.srg.model;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "products")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
@Component
public class Product extends Model {

    @Column(name = "barcode", length = 20, unique = true, nullable = false)
    private String barcode;

    @Column(name = "article", length = 20)
    private String article;

    @Column(name = "name", length = 200, nullable = false)
    private String name;

    @ManyToOne
    @JoinColumn(name = "brand_id", nullable = false)
    private Brand brand;

    @Column(name = "balance", nullable = false, columnDefinition = "INT UNSIGNED")
    private Integer balance;

    @OneToMany(mappedBy = "product", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Document> documentList = new ArrayList<>();

    public Product() {
    }

    public Product(Long id, String barcode, String article, String name, Brand brand, Integer balance) {
        super(id);
        this.barcode = barcode;
        this.article = article;
        this.name = name;
        this.brand = brand;
        this.balance = balance;
    }

    public Product(String barcode, String article, String name, Brand brand, Integer balance) {
        this.barcode = barcode;
        this.article = article;
        this.name = name;
        this.brand = brand;
        this.balance = balance;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getArticle() {
        return article;
    }

    public void setArticle(String article) {
        this.article = article;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public List<Document> getDocumentList() {
        return documentList;
    }

    public void setDocumentList(List<Document> documentList) {
        this.documentList = documentList;
    }

    public void addToDocumentList(Document document) {
        this.documentList.add(document);
    }
}
