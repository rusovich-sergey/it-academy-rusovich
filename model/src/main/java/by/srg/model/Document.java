package by.srg.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;

@Entity
@Table(
        name = "documents",
        uniqueConstraints = @UniqueConstraint(
                columnNames = {"line", "catalog_id"}
        )
)
@Component
public class Document extends Model implements Cloneable {

    @Column(name = "line", nullable = false, columnDefinition = "INT UNSIGNED")
    private Integer line;

    @Column(name = "total", columnDefinition = "INT UNSIGNED DEFAULT '0'")
    private Integer total;

    @Column(name = "price", nullable = false, columnDefinition = "DECIMAL(10,2)")
    private Float price;

    @ManyToOne
    @JoinColumn(name = "catalog_id", nullable = false)
    private Catalog catalog;

    @ManyToOne
    @JoinColumn(name = "product_id", nullable = false)
    private Product product;

    public Document() {
    }

    public Document(Integer line, Integer total, Float price, Catalog catalog, Product product) {
        this.line = line;
        this.total = total;
        this.price = price;
        this.catalog = catalog;
        this.product = product;
    }

    public Integer getLine() {
        return line;
    }

    public void setLine(Integer line) {
        this.line = line;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer order) {
        this.total = order;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Catalog getCatalog() {
        return catalog;
    }

    public void setCatalog(Catalog catalog) {
        this.catalog = catalog;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    @Override
    public Document clone() throws CloneNotSupportedException {
        return (Document) super.clone();
    }

    @Override
    public String toString() {
        return "Document{" +
                "line=" + line +
                ", total=" + total +
                ", price=" + price +
                ", catalog=" + catalog +
                ", product=" + product +
                '}';
    }
}
