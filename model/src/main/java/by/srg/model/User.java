package by.srg.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
@Component
public class User extends Model {

    @Column(name = "full_name", nullable = false, length = 45, unique = true)
    private String fullName;

    @Column(name = "login", nullable = false, length = 45, unique = true)
    private String login;

    @Column(name = "password", nullable = false, length = 50)
    private String password;

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false)
    private Role role;

    @Column(name = "is_active", nullable = false)
    private Boolean isActive;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Catalog> catalogList = new ArrayList<>();

    @Transient //исключаем из Entity
    private Shop thisShop;

    public User() {
    }

    public User(Long id, String fullName, String login, String password, Role role, Boolean isActive) {
        super(id);
        this.fullName = fullName;
        this.login = login;
        this.password = password;
        this.role = role;
        this.isActive = isActive;
    }

    public User(String fullName, String login, String password, Role role, Boolean isActive) {
        this.fullName = fullName;
        this.login = login;
        this.password = password;
        this.role = role;
        this.isActive = isActive;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean active) {
        isActive = active;
    }

    public List<Catalog> getCatalogList() {
        return catalogList;
    }

    public void setCatalogList(List<Catalog> catalogList) {
        this.catalogList = catalogList;
    }

    public void addToCatalogList(Catalog catalog) {
        this.catalogList.add(catalog);
    }

    public Shop getThisShop() {
        return thisShop;
    }

    public void setThisShop(Shop thisShop) {
        this.thisShop = thisShop;
    }

    @Override
    public String toString() {
        return "User{" +
                "fullName='" + fullName + '\'' +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", isActive=" + isActive +
                '}';
    }
}
