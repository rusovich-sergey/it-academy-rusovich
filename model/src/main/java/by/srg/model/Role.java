package by.srg.model;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "roles")
@Component
public class Role extends Model {

    @Column(name = "name", nullable = false, unique = true,
            columnDefinition = "ENUM('Администратор', 'Менеджер', 'Продавец')")
    private String name;

    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY)
    private List<User> userList = new ArrayList<>();

    public Role() {
    }

    public Role(Long id, String name) {
        super(id);
        this.name = name;
    }

    public Role(Long id) {
        super(id);
    }

    public Role(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUserList() {
        return userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public void addToUserList(User user) {
        this.userList.add(user);
    }

    @Override
    public String toString() {
        return "Role{" +
                "name='" + name + '\'' +
                '}';
    }
}
