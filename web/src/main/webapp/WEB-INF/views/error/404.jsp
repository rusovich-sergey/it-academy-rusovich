<%@ page contentType="text/html;charset=UTF-8" %>

<style>
    <%@ include file="../../css/w3.css" %>
</style>

<html>

<head>

    <title>404</title>

    <style>
        body {
            display: flex;
            flex-direction: column;
        }
    </style>

</head>

<header class="w3-container  w3-teal">

    <h2>Ошибка 404!</h2>

</header>

<body>

<jsp:include page="../all/include/_authorization.jsp"/>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="../all/include/_footer.jsp"/>

</footer>

</html>

