<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../../../css/myPageFooter.css" %>
</style>

<c:if test="${sessionScope.user.role.id == 1}">
    <div>
        <nav>
            <ul class="topmenu">
                <li><a href="<%=request.getContextPath()%>/admin">Главная</a></li>
                <li><a href="#" class="submenu-link">Пользователи</a>
                    <ul class="submenu">
                        <li><a href="<%=request.getContextPath()%>/list-users">Посмотреть всех</a></li>
                        <li><a href="<%=request.getContextPath()%>/create-users">Создать</a></li>
                        <li><a href="<%=request.getContextPath()%>/pre-edit-users">Редактировать</a></li>
                        <li><a href="<%=request.getContextPath()%>/pre-delete-users">Удалить</a></li>
                    </ul>
                <li><a href="#" class="submenu-link">Документы</a>
                    <ul class="submenu">
                        <li><a href="<%=request.getContextPath()%>/list-catalogs">Посмотреть все</a></li>
                        <li><a href="<%=request.getContextPath()%>/upload">Создать</a></li>
                        <li><a href="<%=request.getContextPath()%>/pre-edit-catalogs">Редактировать</a></li>
                        <li><a href="<%=request.getContextPath()%>/pre-delete-catalogs">Удалить</a></li>
                    </ul>
                </li>
                <li><a href="#">Контакты</a></li>
            </ul>
        </nav>
    </div>
</c:if>

<c:if test="${user.role.id == 2 || user.role.id == 3}">
    <div>
        <nav>
            <ul class="topmenu">
                <li><a href="<%=request.getContextPath()%>/workspace">Главная</a></li>
                <li><a href="<%=request.getContextPath()%>/list-catalogs">Прайсы</a>
                <li><a href="#">Контакты</a></li>
            </ul>
        </nav>
    </div>
</c:if>
