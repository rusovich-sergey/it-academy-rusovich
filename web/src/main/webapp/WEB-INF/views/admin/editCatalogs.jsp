<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../../css/global.css" %>
    <%@ include file="../../css/w3.css" %>
</style>

<html>

<head>

    <jsp:include page="../all/include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Редактирование каталога</h2>
        <jsp:include page="../all/include/_logoutMenu.jsp"/>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="../all/include/_menu.jsp"/>
    </div>

</header>

<body>

<div class="container">

    <div class="colorText">Для редактирования документа необходимо его выбрать и заполнить только те поля которые
        нужно поменять.
    </div>
    <br>

    <form action="edit-catalog" method="post">

        <label for="catalogId">Документ:</label>
        <select id="catalogId" name="catalogId" required>
            <option></option>
            <c:forEach var="catalog" items="${listCatalogs}">
                <option value=${catalog.id}><c:out value="${catalog.name}"/></option>
            </c:forEach>
        </select>

        <label for="name">Название</label>
        <input type="text" id="name" name="name" placeholder="">

        <label for="status">Статус</label>
        <select id="status" name="status">
            <option value="-1"></option>
            <option value="1">В работе</option>
            <option value="2">Оформлен</option>
            <option value="3">Завершенный</option>
            <option value="4">Новый</option>
            <option value="5">Архивный</option>
        </select>

        <input type="submit" value="Сохранить" id="submit">
    </form>
</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="../all/include/_footer.jsp"/>

</footer>

</html>