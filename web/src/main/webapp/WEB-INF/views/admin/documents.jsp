<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../../css/global.css" %>
    <%@ include file="../../css/w3.css" %>
</style>

<html>

<head>

    <jsp:include page="../all/include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Список документов</h2>
        <jsp:include page="../all/include/_logoutMenu.jsp"/>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="../all/include/_menu.jsp"/>
    </div>

</header>

<body>

<div>

    <%--Таблица для продавца--%>
    <c:if test="${user.role.id == 3}">

        <form method="post" action="<%=request.getContextPath()%>/edit-catalog">

                <%--Если продавец создал новый документ, предлагаем заполнить магазин--%>
            <c:if test="${statusId == 1}">
                <label for="shop">Магазин:</label>
                <select id="shop" name="shop" required>
                    <option></option>
                    <c:forEach var="shop" items="${allShop}">
                        <option value=${shop.id}><c:out value="${shop.name}"/></option>
                    </c:forEach>
                </select>
            </c:if>

            <table class="w3-table-all w3-centered w3-hoverable">
                <thead>
                <tr>
                    <th>№</th>
                    <th>Штрихкод</th>
                    <th>Артикул</th>
                    <th>Наименование</th>
                    <th>Цена</th>
                    <th>Остаток</th>
                    <th>Бренд</th>
                    <th>Заказ</th>
                </tr>
                </thead>
                <c:forEach var="document" items="${documents}">
                    <tr>
                        <td>
                            <c:out value="${document.line}"/>
                        </td>
                        <td>
                            <c:out value="${document.product.barcode}"/>
                        </td>
                        <td>
                            <c:out value="${document.product.article}"/>
                        </td>
                        <td>
                            <c:out value="${document.product.name}"/>
                        </td>
                        <td>
                            <c:out value="${document.price}"/>
                        </td>
                        <td>
                            <c:out value="${document.product.balance}"/>
                        </td>
                        <td>
                            <c:out value="${document.product.brand.name}"/>
                        </td>
                        <td>
                            <input type="number" name="total" autocomplete="off" placeholder="${document.total}">
                        </td>
                    </tr>
                </c:forEach>
            </table>
            <input type="hidden" name="catalogId" value="${catalogId}">
            <input type="hidden" name="status" value="2">
            <input type="submit" value="Сохранить" id="submit">
        </form>
    </c:if>

    <%--Таблица для менеджера--%>
    <c:if test="${user.role.id == 2}">

        <table class="w3-table-all w3-centered ">
            <thead>
            <tr>
                <th>№</th>
                <th>Штрихкод</th>
                <th>Артикул</th>
                <th>Наименование</th>
                <th>Цена</th>
                <th>Остаток</th>
                <th>Бренд</th>
                <th>Заказ</th>
            </tr>
            </thead>

            <tr>

                <td>
                    <table class="w3-table-all w3-hoverable">
                        <c:forEach var="document" items="${documents}">
                            <tr>
                                <td>
                                    <c:out value="${document.line}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>

                <td>
                    <table class="w3-table-all w3-hoverable">
                        <c:forEach var="document" items="${documents}">
                            <tr>
                                <td>
                                    <c:out value="${document.product.barcode}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>

                <td>
                    <table class="w3-table-all w3-hoverable">
                        <c:forEach var="document" items="${documents}">
                            <tr>
                                <td height="36">
                                    <c:out value="${document.product.article}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>

                <td>
                    <table class="w3-table-all w3-hoverable">
                        <c:forEach var="document" items="${documents}">
                            <tr>
                                <td>
                                    <c:out value="${document.product.name}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>

                <td>
                    <table class="w3-table-all w3-hoverable">
                        <c:forEach var="document" items="${documents}">
                            <tr>
                                <td>
                                    <c:out value="${document.price}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>

                <td>
                    <table class="w3-table-all w3-hoverable">
                        <c:forEach var="document" items="${documents}">
                            <tr>
                                <td>
                                    <c:out value="${document.product.balance}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>

                <td>
                    <table class="w3-table-all w3-hoverable">
                        <c:forEach var="document" items="${documents}">
                            <tr>
                                <td>
                                    <c:out value="${document.product.brand.name}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>

                <td>
                    <table class="w3-table-all w3-hoverable">
                        <c:forEach var="document" items="${documents}">
                            <tr>
                                <td>
                                    <c:out value="${document.total}"/>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>

            </tr>
        </table>

        <%--Кнопка смены статуса--%>
        <c:if test="${statusId == 2}">
            <form method="get" action="<%=request.getContextPath()%>/edit-catalog">
                <input type="hidden" name="catalogId" value="${catalogId}">
                <input type="hidden" name="status" value="3">
                <input type="submit" value="Готово">
            </form>
        </c:if>
    </c:if>

    <%--Таблица для админа--%>
    <c:if test="${user.role.id == 1}">

        <table class="w3-table-all w3-centered w3-hoverable">
            <thead>
            <tr>
                <th>№</th>
                <th>Штрихкод</th>
                <th>Артикул</th>
                <th>Наименование</th>
                <th>Цена</th>
                <th>Остаток</th>
                <th>Бренд</th>
                <th>Заказ</th>
            </tr>
            </thead>
            <c:forEach var="document" items="${documents}">
                <tr>
                    <td>
                        <c:out value="${document.line}"/>
                    </td>
                    <td>
                        <c:out value="${document.product.barcode}"/>
                    </td>
                    <td>
                        <c:out value="${document.product.article}"/>
                    </td>
                    <td>
                        <c:out value="${document.product.name}"/>
                    </td>
                    <td>
                        <c:out value="${document.price}"/>
                    </td>
                    <td>
                        <c:out value="${document.product.balance}"/>
                    </td>
                    <td>
                        <c:out value="${document.product.brand.name}"/>
                    </td>
                    <td>
                        <c:out value="${document.total}"/>
                    </td>
                </tr>
            </c:forEach>
        </table>
    </c:if>

</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="../all/include/_footer.jsp"/>

</footer>

</html>