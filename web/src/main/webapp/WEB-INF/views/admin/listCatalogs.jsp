<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../../css/global.css" %>
    <%@ include file="../../css/w3.css" %>
</style>

<html>

<head>

    <jsp:include page="../all/include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Список документов</h2>
        <jsp:include page="../all/include/_logoutMenu.jsp"/>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="../all/include/_menu.jsp"/>
    </div>

</header>

<body>

<div>
    <table class="w3-table-all w3-centered w3-hoverable">
        <thead>
        <tr>
            <th>Статус</th>
            <th>Наименование</th>
            <th>Магазин</th>
            <th>Пользователь</th>
            <th>Дата создания</th>
            <th>Дата редактирования</th>
        </tr>
        </thead>
        <c:forEach var="catalog" items="${listCatalogs}">

            <%--Для продавца--%>
            <c:if test="${sessionScope.user.role.id == 3}">
                <%--Переменная для статуса документа--%>
                <c:set var="statusDoc" value="${catalog.status.id}" scope="request"/>
                <%--Если ((имя пользователя совпадает с именем создателя) и ((статус = work) или (статус = confirmed)))
                или (статус = new)--%>
                <c:if test="${(
                ((sessionScope.user.fullName == catalog.user.fullName) && ((statusDoc == 1) || (statusDoc == 2)))
                ) || (
                (statusDoc == 4)
                )}">
                    <tr onclick="window.location.href='<%=request.getContextPath()%>/list-catalogs/${catalog.id}'">
                        <td>
                            <c:out value="${catalog.status.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.shop.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.user.fullName}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.dateOfCreation}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.dateOfChange}"/>
                        </td>
                    </tr>
                </c:if>
            </c:if>

            <%--Для менеджера--%>
            <c:if test="${sessionScope.user.role.id == 2}">
                <%--Переменная для статуса документа--%>
                <c:set var="statusDoc" value="${catalog.status.id}" scope="request"/>
                <%--Если (статус = confirmed) или (статус = completed))--%>
                <c:if test="${
                ((statusDoc == 2) || (statusDoc == 3))
                }">
                    <tr onclick="window.location.href='<%=request.getContextPath()%>/list-catalogs/${catalog.id}'">
                        <td>
                            <c:out value="${catalog.status.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.shop.name}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.user.fullName}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.dateOfCreation}"/>
                        </td>
                        <td>
                            <c:out value="${catalog.dateOfChange}"/>
                        </td>
                    </tr>
                </c:if>
            </c:if>

            <%--Для админа--%>
            <c:if test="${sessionScope.user.role.id == 1}">
                <tr onclick="window.location.href='<%=request.getContextPath()%>/list-catalogs/${catalog.id}'">
                    <td>
                        <c:out value="${catalog.status.name}"/>
                    </td>
                    <td>
                        <c:out value="${catalog.name}"/>
                    </td>
                    <td>
                        <c:out value="${catalog.shop.name}"/>
                    </td>
                    <td>
                        <c:out value="${catalog.user.fullName}"/>
                    </td>
                    <td>
                        <c:out value="${catalog.dateOfCreation}"/>
                    </td>
                    <td>
                        <c:out value="${catalog.dateOfChange}"/>
                    </td>
                </tr>
            </c:if>

        </c:forEach>
    </table>
</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="../all/include/_footer.jsp"/>

</footer>

</html>