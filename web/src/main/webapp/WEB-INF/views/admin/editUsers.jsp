<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../../css/global.css" %>
    <%@ include file="../../css/w3.css" %>
</style>

<html>

<head>

    <jsp:include page="../all/include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Редактирование пользователя</h2>
        <jsp:include page="../all/include/_logoutMenu.jsp"/>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="../all/include/_menu.jsp"/>
    </div>

</header>

<body>

<div class="container">

    <div class="colorText">Для редактирования пользователя необходимо его выбрать и заполнить только те поля которые
        нужно поменять.
    </div>
    <br>

    <form action="edit-user" method="post">

        <label for="userId">Пользователь:</label>
        <select id="userId" name="userId" required>
            <option></option>
            <c:forEach var="user" items="${listUsers}">
                <option value=${user.id}><c:out value="${user.fullName}"/></option>
            </c:forEach>
        </select>

        <label for="fullName">ФИО</label>
        <input type="text" id="fullName" name="fullName" placeholder="">

        <label for="login">Имя пользователя</label>
        <input type="text" id="login" name="login" placeholder="">

        <label for="password">Пароль</label>
        <input type="text" id="password" name="password" placeholder="">

        <label for="role">Роль</label>
        <select id="role" name="role">
            <option value="-1"></option>
            <option value="2">Менеджер</option>
            <option value="1">Админ</option>
            <option value="3">Продавец</option>
        </select>

        <label for="isActive">Is Active</label>
        <select id="isActive" name="isActive">
            <option value=""></option>
            <option value="false">False</option>
            <option value="true">True</option>
        </select>

        <input type="submit" value="Сохранить" id="submit">
    </form>
</div>

<%--    <script>--%>
<%--        $( "select" )--%>
<%--            .change(function () {--%>
<%--                var str = "";--%>
<%--                $( "select option:selected" ).each(function() {--%>
<%--                    str += $( this ).text() + " ";--%>
<%--                });--%>
<%--                $( "div" ).text( str );--%>
<%--            })--%>
<%--            .change();--%>
<%--    </script>--%>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="../all/include/_footer.jsp"/>

</footer>

</html>