<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../../css/global.css" %>
    <%@ include file="../../css/w3.css" %>
</style>

<html>

<head>

    <jsp:include page="../all/include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Это админ панель</h2>
        <jsp:include page="../all/include/_logoutMenu.jsp"/>
    </div>

    <div>
        <c:if test="${nameFile != null}">
            <p class="colorText"> Загружен документ: ${nameFile}</p>
        </c:if>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="../all/include/_menu.jsp"/>
    </div>

</header>

<body>

<%--<script type="text/javascript">--%>

<%--    function copytable(el) {--%>
<%--        var urlField = document.getElementById(el)--%>
<%--        var range = document.createRange()--%>
<%--        range.selectNode(urlField)--%>
<%--        window.getSelection().addRange(range)--%>
<%--        document.execCommand('copy')--%>
<%--    }--%>

<%--</script>--%>

<%--<input type=button value="Copy to Clipboard" onClick="copytable('stats')">--%>


<%--<table id=stats class="w3-table-all w3-centered w3-hoverable" rules="all">--%>

<%--    <tr>--%>
<%--        <th>№</th>--%>
<%--        <th>Штрихкод</th>--%>
<%--        <th>Артикул</th>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--        <td>1</td>--%>
<%--        <td>4707520809643</td>--%>
<%--        <td>1111</td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--        <td>2</td>--%>
<%--        <td>4707259369032</td>--%>
<%--        <td>3333</td>--%>
<%--    </tr>--%>
<%--    <tr>--%>
<%--        <td>3</td>--%>
<%--        <td>4700725693702</td>--%>
<%--        <td>4444</td>--%>
<%--    </tr>--%>
<%--</table>--%>


<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="../all/include/_footer.jsp"/>

</footer>

</html>
