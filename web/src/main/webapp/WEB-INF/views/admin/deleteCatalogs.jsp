<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../../css/global.css" %>
    <%@ include file="../../css/w3.css" %>
</style>

<html>

<head>

    <jsp:include page="../all/include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Удаление документов</h2>
        <jsp:include page="../all/include/_logoutMenu.jsp"/>
    </div>
    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="../all/include/_menu.jsp"/>
    </div>

</header>

<body>

<div class="container">
    <div class="colorText">
        Для удаления документа необходимо его выбрать в выпадающем списке.
    </div>
    <br>

    <form action="delete-catalog" method="post">

        <label for="catalogId">Документ:</label>
        <select id="catalogId" name="catalogId" required>
            <option></option>
            <c:forEach var="catalog" items="${listCatalogs}">
                <option value=${catalog.id}><c:out value="${catalog.name}"/></option>
            </c:forEach>
        </select>

        <input type="submit" value="Удалить" id="submit">
    </form>
</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="../all/include/_footer.jsp"/>

</footer>

</html>