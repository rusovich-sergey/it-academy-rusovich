<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<style>
    <%@ include file="../../css/global.css" %>
    <%@ include file="../../css/w3.css" %>
</style>

<html>

<head>

    <jsp:include page="../all/include/_title.jsp"/>

</head>

<header>

    <div class="w3-container  w3-teal">
        <h2>Создание пользователя</h2>
        <jsp:include page="../all/include/_logoutMenu.jsp"/>
    </div>

    <div class="w3-container" style="background-color: #00ffff">
        <jsp:include page="../all/include/_menu.jsp"/>
    </div>

</header>

<body>

<div class="container">
    <form method="post">

        <label for="fullName">Full Name</label>
        <input type="text" id="fullName" name="fullName" placeholder="" required>

        <label for="login">Login</label>
        <input type="text" id="login" name="login" placeholder="" required>

        <label for="password">Password</label>
        <input type="text" id="password" name="password" placeholder="" required>

        <label for="role">Role</label>
        <select id="role" name="role">
            <option value="2">Manager</option>
            <option value="1">Admin</option>
            <option value="3">Seller</option>
        </select>

        <label for="isActive">Is Active</label>
        <select id="isActive" name="isActive">
            <option value="false">False</option>
            <option value="true">True</option>
        </select>

        <input type="submit" value="Создать">
    </form>
</div>

<div class="w3-container" style="flex-grow: 1">
</div>

</body>

<footer class="w3-container w3-teal">

    <jsp:include page="../all/include/_footer.jsp"/>

</footer>

</html>