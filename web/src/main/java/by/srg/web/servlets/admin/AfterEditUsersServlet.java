package by.srg.web.servlets.admin;

import by.srg.model.User;
import by.srg.services.RoleCrud;
import by.srg.services.UserCrud;
import by.srg.services.utils.PasswordUtil;
import by.srg.web.servlets.SpringHttpServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/edit-user")
public class AfterEditUsersServlet extends SpringHttpServlet {

    @Autowired
    private UserCrud userCrud;
    @Autowired
    private RoleCrud roleCrud;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        String isActiveParameter = req.getParameter("isActive");

        User user = null;
        try {
            user = new User(
                    Long.valueOf(req.getParameter("userId")),
                    req.getParameter("fullName"),
                    req.getParameter("login"),
                    PasswordUtil.getHashString(req.getParameter("password")),
//                    req.getParameter("password"),
                    roleCrud.read(Long.parseLong(req.getParameter("role"))),
                    isActiveParameter.equals("") ? null : Boolean.valueOf(isActiveParameter)
            );

            userCrud.edit(user);

        } catch (Exception e) {
            e.printStackTrace();
        }
        getServletContext().getRequestDispatcher("/pre-edit-users").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
