package by.srg.web.servlets.admin;

import by.srg.services.utils.ParserXlsUtil;
import by.srg.web.servlets.SpringHttpServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;

@WebServlet("/upload")
@MultipartConfig(
        fileSizeThreshold = 1024 * 1024 * 2, // После какого размера временно сохранит на диск, default 0, 2MB
        maxFileSize = 1024 * 1024 * 10, // Размер одного файла 10MB
        maxRequestSize = 1024 * 1024 * 50 // Общий объем всех файлов 50MB
)
public class UploadToDbServlet extends SpringHttpServlet {

    @Autowired
    private ParserXlsUtil parserXlsUtil;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/views/admin/uploadToDb.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        Part file = req.getPart("file");

        String nameFile = extractFileName(file);

        parserXlsUtil.readBook(file.getInputStream(), nameFile);

        req.setAttribute("nameFile", nameFile);

        getServletContext().getRequestDispatcher("/WEB-INF/views/admin/adminPanel.jsp").forward(req, resp);
    }

    private String extractFileName(Part part) {
        String contentDisp = part.getHeader("content-disposition");
        String[] items = contentDisp.split(";");
        //form-data; name="file"; filename="*.xlsx"

        for (String s : items) {
            if (s.trim().startsWith("filename")) {
                return s.substring(s.indexOf("=") + 2, s.length() - 6);
            }
        }
        return null;
    }
}
