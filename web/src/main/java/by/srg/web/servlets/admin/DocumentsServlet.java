package by.srg.web.servlets.admin;

import by.srg.model.Catalog;
import by.srg.model.Document;
import by.srg.model.User;
import by.srg.services.CatalogCrud;
import by.srg.services.DocumentCrud;
import by.srg.web.servlets.SpringHttpServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/list-catalogs/*")
public class DocumentsServlet extends SpringHttpServlet {

    @Autowired
    private DocumentCrud documentCrud;
    @Autowired
    private CatalogCrud catalogCrud;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        User user = (User) req.getSession().getAttribute("user");

        List<Document> documents = null;
        Catalog catalog = null;

        String path = req.getPathInfo();
        Long catalogId = Long.valueOf(path.substring(1));

        try {
            catalog = catalogCrud.read(catalogId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Long statusId = catalog.getStatus().getId();

        //клонирование документа
        if (user.getRole().getId() == 3) {
            if (statusId == 4) {
                try {
                    catalogId = catalogCrud.clone(catalog, user);
                    statusId = 1L;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        try {
            documents = documentCrud.getAllByIdCatalog(catalogId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        req.setAttribute("statusId", statusId);
        req.setAttribute("catalogId", catalogId);
        req.setAttribute("documents", documents);
        getServletContext().getRequestDispatcher("/WEB-INF/views/admin/documents.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
