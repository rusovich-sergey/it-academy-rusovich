package by.srg.web.servlets;

import by.srg.model.Shop;
import by.srg.model.User;
import by.srg.services.AuthorizationService;
import by.srg.services.ShopCrud;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Controller
@WebServlet("/login")
public class LoginServlet extends SpringHttpServlet {

    @Autowired
    private AuthorizationService authorizationService;
    @Autowired
    private ShopCrud shopCrud;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        getServletContext().getRequestDispatcher("/WEB-INF/views/all/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");
        HttpSession session = req.getSession(true);
        User user;
        List<Shop> allShop = null;

        try {
            allShop = shopCrud.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        user = authorizationService.authorize(userName, password);

        String contextPath = req.getContextPath();

        if (user != null) {
            long roleId = user.getRole().getId();
            session.setAttribute("user", user);
            session.setAttribute("allShop", allShop);
            session.setAttribute("userId", user.getId());

            if (user.getIsActive()) {
                if (roleId == 1) {
                    resp.sendRedirect(contextPath + "/admin");
                } else if (roleId == 2 || roleId == 3) {
                    resp.sendRedirect(contextPath + "/workspace");
                }
            } else {
                req.setAttribute("errorMessage", "Пользователь не активен");
                req.getRequestDispatcher("/WEB-INF/views/error/401.jsp").forward(req, resp);
            }

        } else {
            req.setAttribute("errorMessage", "Неправильные имя пользователя или пароль");
            req.getRequestDispatcher("/WEB-INF/views/error/401.jsp").forward(req, resp);
        }
    }
}
