package by.srg.web.servlets.admin;

import by.srg.model.User;
import by.srg.services.RoleCrud;
import by.srg.services.UserCrud;
import by.srg.services.utils.PasswordUtil;
import by.srg.web.servlets.SpringHttpServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(
        value = "/create-users",
        initParams = {
                @WebInitParam(name = "firstEntry", value = "true")
        }
)
public class CreateUsersServlet extends SpringHttpServlet {

    @Autowired
    private UserCrud userCrud;
    @Autowired
    private RoleCrud roleCrud;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        req.setCharacterEncoding("UTF-8");
        Boolean firstEntry = (Boolean) session.getAttribute("firstEntry");

        if (firstEntry != null) {
            User user = null;
            Long id = null;

            try {
                user = new User(
//                        0L,
                        req.getParameter("fullName"),
                        req.getParameter("login"),
                        PasswordUtil.getHashString(req.getParameter("password")),
//                        req.getParameter("password"),
                        roleCrud.read(Long.parseLong(req.getParameter("role"))),
                        Boolean.valueOf(req.getParameter("isActive"))
                );

                id = userCrud.create(user);

            } catch (Exception e) {
                e.printStackTrace();
            }
            req.setAttribute("id", id);
        }

        session.setAttribute("firstEntry", false);

        getServletContext().getRequestDispatcher("/WEB-INF/views/admin/createUsers.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
