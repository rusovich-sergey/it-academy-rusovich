package by.srg.web.servlets.admin;

import by.srg.model.Catalog;
import by.srg.services.CatalogCrud;
import by.srg.web.servlets.SpringHttpServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/list-catalogs")
public class ListCatalogsServlet extends SpringHttpServlet {

    @Autowired
    private CatalogCrud catalogCrud;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Catalog> catalogs = null;

        try {
            catalogs = catalogCrud.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        req.setAttribute("listCatalogs", catalogs);
        getServletContext().getRequestDispatcher("/WEB-INF/views/admin/listCatalogs.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
