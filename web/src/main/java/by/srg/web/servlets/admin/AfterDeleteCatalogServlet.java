package by.srg.web.servlets.admin;

import by.srg.services.CatalogCrud;
import by.srg.web.servlets.SpringHttpServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delete-catalog")
public class AfterDeleteCatalogServlet extends SpringHttpServlet {

    @Autowired
    private CatalogCrud catalogCrud;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Long id = null;

        id = Long.valueOf(req.getParameter("catalogId"));

        try {
            catalogCrud.delete(id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        getServletContext().getRequestDispatcher("/pre-delete-catalogs").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
