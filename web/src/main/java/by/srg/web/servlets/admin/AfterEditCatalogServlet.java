package by.srg.web.servlets.admin;

import by.srg.model.Catalog;
import by.srg.model.User;
import by.srg.services.CatalogCrud;
import by.srg.services.DocumentCrud;
import by.srg.services.ShopCrud;
import by.srg.services.StatusCrud;
import by.srg.web.servlets.SpringHttpServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/edit-catalog")
public class AfterEditCatalogServlet extends SpringHttpServlet {

    @Autowired
    private CatalogCrud catalogCrud;
    @Autowired
    private StatusCrud statusCrud;
    @Autowired
    private ShopCrud shopCrud;
    @Autowired
    private DocumentCrud documentCrud;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("UTF-8");

        User user = (User) req.getSession().getAttribute("user");
        Catalog catalog = null;
        String shopId = req.getParameter("shop");
        String[] totals = req.getParameterValues("total");
        Long catalogId = Long.valueOf(req.getParameter("catalogId"));

        try {
            catalog = new Catalog(
                    catalogId,
                    req.getParameter("name"),
                    statusCrud.read(Long.parseLong(req.getParameter("status")))
            );

            if (!(shopId == null)) {
                catalog.setShop(shopCrud.read(Long.parseLong(shopId)));
            }

            catalogCrud.edit(catalog);

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (!(totals == null)) {
            documentCrud.editDocumentList(totals, catalogId);
        }

        if ((user.getRole().getId() == 2L) || (user.getRole().getId() == 3L)) {
            getServletContext().getRequestDispatcher("/list-catalogs").forward(req, resp);
        } else {
            getServletContext().getRequestDispatcher("/pre-edit-catalogs").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
