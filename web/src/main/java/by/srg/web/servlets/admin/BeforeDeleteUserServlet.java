package by.srg.web.servlets.admin;

import by.srg.model.User;
import by.srg.services.UserCrud;
import by.srg.web.servlets.SpringHttpServlet;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/pre-delete-users")
public class BeforeDeleteUserServlet extends SpringHttpServlet {

    @Autowired
    private UserCrud userCrud;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<User> users = null;

        try {
            users = userCrud.getAll();
        } catch (Exception e) {
            e.printStackTrace();
        }

        req.setAttribute("listUsers", users);

        getServletContext().getRequestDispatcher("/WEB-INF/views/admin/deleteUsers.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
