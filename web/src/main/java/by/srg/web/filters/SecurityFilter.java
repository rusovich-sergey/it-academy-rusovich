package by.srg.web.filters;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@WebFilter(
        urlPatterns = "/*",
        filterName = "SecurityFilter",
        displayName = "DISPLAY NAME SECURITY FILTER"
)

public class SecurityFilter implements Filter {

    private static final Set<String> ALLOWED_PATHS = Collections.unmodifiableSet(
            new HashSet<>(Arrays.asList("/", "/login")));

    public SecurityFilter() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        if (bypassFilter(req) || hasSession(req)) {
            chain.doFilter(req, resp);
        } else {
            resp.sendRedirect(req.getContextPath());
        }
    }

    @Override
    public void init(FilterConfig fConfig) {
    }

    @Override
    public void destroy() {
    }

    private boolean bypassFilter(HttpServletRequest request) {
        String contextPath = request.getContextPath();
        String path = request.getRequestURI().replaceFirst(contextPath, "");
        return ALLOWED_PATHS.stream().anyMatch(path::equals);
    }

    private boolean hasSession(HttpServletRequest request) {
        HttpSession session = request.getSession();
        return session != null && session.getAttribute("userId") != null;
    }
}
