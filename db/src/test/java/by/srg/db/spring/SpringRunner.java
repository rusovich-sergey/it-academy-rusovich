package by.srg.db.spring;

import by.srg.db.dao.impl.spring.SpringUserDao;
import by.srg.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SpringRunner {

    //    @Autowired
//    public static ShopRepository shopRepository;
    @Autowired
    static SpringUserDao springUserDao;


    public static void main(String[] args) throws Exception {
//        ClassPathXmlApplicationContext context =
//                new ClassPathXmlApplicationContext("classpath*:spring-context.xml");
//
//        ShopRepository shopRepository = context.getBean("shopRepository", ShopRepository.class);
//        SpringUserDao springUserDao = context.getBean(SpringUserDao.class);


        System.out.println(springUserDao.get(1L));

        Long id = springUserDao.getIdByLogin("qwer");
        System.out.println(id);


        List<User> userList = springUserDao.getAll();

        for (User u : userList) {
            System.out.println(u);
        }

        List<User> result = new ArrayList<>();

//        Iterable it = new Iterable() {
//            @Override
//            public Iterator iterator() {
//                return null;
//            }
//        }
//
//        userRepository.findAll().forEach(result::add);
//        return result;


//        Shop result = null;
//        Optional<Shop> byId = shopRepository.findById(1L);
//        result = byId.get();
//
//        System.out.println(result);

    }

}

