package by.srg.db.connection;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JdbcProviderTest {

    private static final String SELECT_SQL_STATEMENT = "SELECT * FROM users";


    //    @Test
    public void testConnection() {
        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;

        try {
            connection = JdbcProvider.getInstance().getConnection();
            statement = connection.createStatement();
            resultSet = statement.executeQuery(SELECT_SQL_STATEMENT);

            while (resultSet.next()) {
                System.out.println(String.format("%s  %s  %s  %s",
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4)
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException ignored) {
                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ignored) {
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException ignored) {
                }
            }
        }
    }

    //    @Test
    public void testConnectionTryWithResources() {
        try (Connection connection = JdbcProvider.getInstance().getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(SELECT_SQL_STATEMENT)) {
            while (resultSet.next()) {
                System.out.println(String.format("%s  %s  %s  %s",
                        resultSet.getInt(1),
                        resultSet.getString(2),
                        resultSet.getString(3),
                        resultSet.getString(4)
                ));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
