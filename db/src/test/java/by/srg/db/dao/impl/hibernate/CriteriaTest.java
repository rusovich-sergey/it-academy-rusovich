package by.srg.db.dao.impl.hibernate;

import by.srg.db.dao.BrandDao;
import by.srg.db.dao.DocumentDao;
import by.srg.db.dao.RoleDao;
import by.srg.db.dao.UserDao;
import by.srg.model.Document;
import by.srg.model.Role;
import by.srg.model.User;

import java.util.List;

public class CriteriaTest {

    //    @Test
    public void delete() throws Exception {
        UserDao dao = HibernateUserDao.getInstance();

        dao.delete(2L);


        System.out.println("Готово");

    }

    //    @Test
    public void getAll() throws Exception {
        UserDao dao = HibernateUserDao.getInstance();
        List<User> list = dao.getAll();

//        list.forEach(System.out::println);

        for (User u : list) {
            System.out.println("-----------------");
            System.out.println(u);

        }
    }

//    @Test
//    public void create() throws Exception {
//        UserDao dao = HibernateUserDao.getInstance();
//
//        User user = new User("name3", "login3", "pass3", new Role(3L), true);
//
//        dao.create(user);
//
//        List<User> list = dao.getAll();
//        for (User u : list) {
//            System.out.println("-----------------");
//            System.out.println(u);
//
//        }
//    }

    //    @Test
    public void get() throws Exception {
        UserDao dao = HibernateUserDao.getInstance();

        User user = dao.get(2L);

        System.out.println("-----------------" + user);

    }

    //    @Test
    public void getIdByLogin() throws Exception {
        UserDao dao = HibernateUserDao.getInstance();
        Long id = dao.getIdByLogin("vasya");

        System.out.println(id);
    }

    //    @Test
    public void getRole() throws Exception {
        RoleDao dao = HibernateRoleDao.getInstance();

        Role role = dao.get(2L);

        System.out.println("-----------------" + role);

    }

    //    @Test
    public void getAllByIdCatalog() {
        DocumentDao dao = HibernateDocumentDao.getInstance();
        try {
            List<Document> list = dao.getAllByIdCatalog(1L);

            for (Document d : list) {
                System.out.println(d);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    @Test
    public void getBrandIdByName() throws Exception {
        BrandDao dao = HibernateBrandDao.getInstance();

        Long id = dao.getIdByName("KAPOUSS");
        System.out.println(id);
    }
}
