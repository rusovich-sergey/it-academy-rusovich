package by.srg.db.hibernate;

import by.srg.db.connection.HibernateProvider;
import by.srg.model.User;
import org.hibernate.Session;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class TestCriteria {
    private static Session session;

    //    @Test
    public void getAll() throws Exception {
        List<User> result = new ArrayList<>();

        session = HibernateProvider.getSessionFactory().openSession();
        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<User> query = cb.createQuery(User.class);
        Root<User> userRoot = query.from(User.class);
        query.select(userRoot);

        result = session.createQuery(query).getResultList();

        for (User u : result) {
            System.out.println(u);
        }
    }
}
