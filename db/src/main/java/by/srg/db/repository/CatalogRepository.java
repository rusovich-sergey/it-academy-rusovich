package by.srg.db.repository;


import by.srg.model.Catalog;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CatalogRepository extends CrudRepository<Catalog, Long> {

    @Override
    <S extends Catalog> S save(S s);

    @Override
    Optional<Catalog> findById(Long aLong);

    @Override
    Iterable<Catalog> findAll();

    @Override
    void delete(Catalog catalog);
}
