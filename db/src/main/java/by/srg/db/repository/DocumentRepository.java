package by.srg.db.repository;


import by.srg.model.Document;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DocumentRepository extends CrudRepository<Document, Long> {

    @Override
    <S extends Document> S save(S s);

    @Override
    Optional<Document> findById(Long aLong);

    @Override
    Iterable<Document> findAll();

    @Override
    void deleteById(Long aLong);

    @Query("select d from Document d where d.catalog.id = :id")
    Iterable<Document> getAllByIdCatalog(@Param("id") Long id);
}
