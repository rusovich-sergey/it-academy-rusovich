package by.srg.db.repository;


import by.srg.model.Shop;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ShopRepository extends CrudRepository<Shop, Long> {

    @Override
    <S extends Shop> S save(S s);

    @Override
    Optional<Shop> findById(Long aLong);

    @Override
    Iterable<Shop> findAll();

    @Override
    void deleteById(Long aLong);
}
