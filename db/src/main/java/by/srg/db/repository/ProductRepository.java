package by.srg.db.repository;


import by.srg.model.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {

    @Override
    <S extends Product> S save(S s);

    @Override
    Optional<Product> findById(Long aLong);

    @Override
    Iterable<Product> findAll();

    @Override
    void deleteById(Long aLong);

    @Query("select p.id from Product p where p.barcode = :barcode")
    Long getIdByBarcode(@Param("barcode") String barcode);
}
