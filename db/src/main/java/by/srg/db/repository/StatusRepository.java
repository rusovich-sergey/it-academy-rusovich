package by.srg.db.repository;


import by.srg.model.Status;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StatusRepository extends CrudRepository<Status, Long> {

    @Override
    <S extends Status> S save(S s);

    @Override
    Optional<Status> findById(Long aLong);

    @Override
    Iterable<Status> findAll();

    @Override
    void deleteById(Long aLong);
}
