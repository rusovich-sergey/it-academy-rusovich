package by.srg.db.repository;


import by.srg.model.Brand;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface BrandRepository extends CrudRepository<Brand, Long> {

    @Override
    <S extends Brand> S save(S s);

    @Override
    Optional<Brand> findById(Long aLong);

    @Override
    Iterable<Brand> findAll();

    @Override
    void deleteById(Long aLong);

    @Query("select b.id from Brand b where b.name = :name")
//    Long getIdByLogin(@Param("name") String name);
    Long getIdByName(@Param("name") String name);
}
