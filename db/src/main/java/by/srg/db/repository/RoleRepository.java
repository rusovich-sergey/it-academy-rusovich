package by.srg.db.repository;


import by.srg.model.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoleRepository extends CrudRepository<Role, Long> {

    @Override
    <S extends Role> S save(S s);

    @Override
    Optional<Role> findById(Long aLong);

    @Override
    Iterable<Role> findAll();

    @Override
    void deleteById(Long aLong);
}
