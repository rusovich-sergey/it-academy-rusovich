package by.srg.db.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class JdbcProvider {
    private static final Logger LOGGER = LogManager.getLogger();

    private static final String PROPERTIES_FILE_NAME = "connection.properties";

    private static final String JDBC_DRIVER_NAME = "jdbc.driverClassName";
    private static final String JDBC_URL = "jdbc.url";
    private static final String JDBC_USERNAME = "jdbc.username";
    private static final String JDBC_PASSWORD = "jdbc.password";

    private static final JdbcProvider jdbcProvider = new JdbcProvider();

    private JdbcProvider() {
    }

    public static JdbcProvider getInstance() {
        return jdbcProvider;
    }


    public Connection getConnection() throws IOException, ClassNotFoundException, SQLException {

        Connection connection;

        InputStream in = JdbcProvider.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE_NAME);
        Properties props = new Properties();
        props.load(in);

        String jdbcDriverName = (String) props.get(JDBC_DRIVER_NAME);
        String jdbcUrl = (String) props.get(JDBC_URL);
        String jdbcUsername = (String) props.get(JDBC_USERNAME);
        String jdbcPassword = (String) props.get(JDBC_PASSWORD);

        Class.forName(jdbcDriverName);

        connection = DriverManager.getConnection(jdbcUrl, jdbcUsername, jdbcPassword);

        if (connection == null) {
            LOGGER.error("Неудалось создать соеденение с БД.");
        } else {
            LOGGER.info("Соеденение с БД установлено.");
        }
        return connection;
    }

}
