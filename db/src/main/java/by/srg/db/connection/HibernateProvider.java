package by.srg.db.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateProvider {
    private static final Logger LOGGER = LogManager.getLogger(HibernateProvider.class);
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        LOGGER.info("Init SessionFactory");
        if (sessionFactory == null) {
            synchronized (HibernateProvider.class) {
                if (sessionFactory == null) {
                    sessionFactory = new Configuration().configure().buildSessionFactory();
                }
            }
        }
        return sessionFactory;
    }

    public static void close() {
        LOGGER.info("Closing SessionFactory");
        if (sessionFactory != null) {
            sessionFactory.close();
        }
    }
}
