package by.srg.db.dao;

import by.srg.model.Role;

public interface RoleDao extends DAO<Role> {

}
