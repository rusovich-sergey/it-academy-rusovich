package by.srg.db.dao.impl.jdbc;

import by.srg.db.dao.ProductDao;
import by.srg.model.Product;

import java.util.List;

public class JdbcProductDao implements ProductDao {
    private static ProductDao productDao;

    private JdbcProductDao() {

    }

    public static ProductDao getInstance() {
        if (productDao == null) {
            synchronized (JdbcProductDao.class) {
                if (productDao == null) {
                    productDao = new JdbcProductDao();
                }
            }
        }
        return productDao;
    }

    @Override
    public Long create(Product record) throws Exception {
        return null;
    }

    @Override
    public Product get(Long id) throws Exception {
        return null;
    }

    @Override
    public List<Product> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Product record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }

    @Override
    public Long getIdByBarcode(String barcode) throws Exception {
        return null;
    }

    enum Query {
        GET("SELECT..."),
        INSERT("INSERT..."),
        DELETE("DELETE..."),
        UPDATE("UPDATE...");

        private static final String TABLE = "products";

        private String QUERY;

        Query(String QUERY) {
            this.QUERY = QUERY;
        }
    }
}
