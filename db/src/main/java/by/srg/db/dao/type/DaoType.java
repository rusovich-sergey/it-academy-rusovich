package by.srg.db.dao.type;

public enum DaoType {
    JDBC,
    HIBERNATE,
    SPRING
}
