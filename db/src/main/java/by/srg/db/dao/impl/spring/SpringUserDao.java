package by.srg.db.dao.impl.spring;

import by.srg.db.dao.UserDao;
import by.srg.db.repository.UserRepository;
import by.srg.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SpringUserDao implements UserDao {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Long create(User record) throws Exception {
        return userRepository.save(record).getId();
    }

    @Override
    public User get(Long id) throws Exception {
        return userRepository.findById(id).orElseThrow(Exception::new);
    }

    @Override
    public List<User> getAll() throws Exception {
        List<User> result = new ArrayList<>();
        userRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public void update(User record) throws Exception {
        userRepository.save(record);
    }

    @Override
    public void delete(Long id) throws Exception {
        userRepository.deleteById(id);
    }

    @Override
    public Long getIdByLogin(String login) throws Exception {
        return userRepository.getIdByLogin(login);
    }

    @Override
    public User getByLogin(String login) throws Exception {
        return userRepository.getByLogin(login);
    }
}
