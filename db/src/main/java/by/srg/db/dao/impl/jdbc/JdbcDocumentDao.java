package by.srg.db.dao.impl.jdbc;

import by.srg.db.dao.DocumentDao;
import by.srg.model.Document;

import java.util.List;

public class JdbcDocumentDao implements DocumentDao {
    private static DocumentDao documentDao;

    private JdbcDocumentDao() {

    }

    public static DocumentDao getInstance() {
        if (documentDao == null) {
            synchronized (JdbcDocumentDao.class) {
                if (documentDao == null) {
                    documentDao = new JdbcDocumentDao();
                }
            }
        }
        return documentDao;
    }

    @Override
    public Long create(Document record) throws Exception {
        return null;
    }

    @Override
    public Document get(Long id) throws Exception {
        return null;
    }

    @Override
    public List<Document> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Document record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }

    @Override
    public List<Document> getAllByIdCatalog(Long id) throws Exception {
        return null;
    }

    enum Query {
        GET("SELECT..."),
        INSERT("INSERT..."),
        DELETE("DELETE..."),
        UPDATE("UPDATE...");

        private static final String TABLE = "documents";

        private String QUERY;

        Query(String QUERY) {
            this.QUERY = QUERY;
        }
    }
}
