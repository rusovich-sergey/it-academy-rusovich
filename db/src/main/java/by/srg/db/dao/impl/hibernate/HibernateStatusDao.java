package by.srg.db.dao.impl.hibernate;

import by.srg.db.connection.HibernateProvider;
import by.srg.db.dao.StatusDao;
import by.srg.model.Status;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class HibernateStatusDao implements StatusDao {

    private static final Logger LOGGER = LogManager.getLogger();

    private static SessionFactory sessionFactory = HibernateProvider.getSessionFactory();

    private static StatusDao statusDao;


    public HibernateStatusDao() {
    }

    public static StatusDao getInstance() {
        if (statusDao == null) {
            synchronized (HibernateStatusDao.class) {
                if (statusDao == null) {
                    statusDao = new HibernateStatusDao();
                }
            }
        }
        return statusDao;
    }

    @Override
    public Long create(Status record) throws Exception {
        return null;
    }

    @Override
    public Status get(Long id) throws Exception {
        Status result = null;

        try (Session session = sessionFactory.openSession()) {
            final CriteriaBuilder cb = session.getCriteriaBuilder();
            final CriteriaQuery<Status> query = cb.createQuery(Status.class);
            final Root<Status> root = query.from(Status.class);

            Transaction transaction = session.beginTransaction();
            query.select(root).where(cb.equal(root.get("id"), id));
            result = session.createQuery(query).uniqueResult();
            transaction.commit();
        }
        return result;
    }

    @Override
    public List<Status> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Status record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }
}
