package by.srg.db.dao;

import by.srg.model.Brand;

public interface BrandDao extends DAO<Brand> {
    Long getIdByName(String name) throws Exception;

}
