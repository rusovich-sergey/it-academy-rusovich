package by.srg.db.dao.impl.jdbc;

import by.srg.db.connection.JdbcProvider;
import by.srg.db.dao.UserDao;
import by.srg.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class JdbcUserDao implements UserDao {
    private static final Logger LOGGER = LogManager.getLogger();
    private static UserDao userDao;

    private JdbcUserDao() {
    }

    public static UserDao getInstance() {
        if (userDao == null) {
            synchronized (JdbcUserDao.class) {
                if (userDao == null) {
                    userDao = new JdbcUserDao();
                }
            }
        }
        return userDao;
    }

    private JdbcRoleDao jdbcRoleDao = JdbcRoleDao.getInstance();

    @Override
    public Long create(User record) throws Exception {
        Long result = null;

        try (Connection connection = JdbcProvider.getInstance().getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(Query.CREATE.QUERY)) {
                preparedStatement.setString(1, record.getFullName());
                preparedStatement.setString(2, record.getLogin());
                preparedStatement.setString(3, record.getPassword());
                preparedStatement.setLong(4, record.getRole().getId());
                preparedStatement.setBoolean(5, record.getIsActive());

                if (preparedStatement.executeUpdate() > 0) {
                    result = getIdByLogin(record.getLogin());
                    LOGGER.info("Был создан пользователь '" + record.getLogin() + "'");
                } else {
                    LOGGER.error("Пользователь не был создан!");
                }
            }
        }
        return result;
    }

    @Override
    public User get(Long id) throws Exception {
        User result = null;
        try (Connection connection = JdbcProvider.getInstance().getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET.QUERY)) {
                preparedStatement.setLong(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        result = new User(
                                id,
                                resultSet.getString("full_name"),
                                resultSet.getString("login"),
                                resultSet.getString("password"),
                                jdbcRoleDao.get(resultSet.getLong("role_id")),
                                resultSet.getBoolean("is_active")
                        );
                        LOGGER.info("Из БД получен пользователь " + result.getLogin());
                    }
                }
            }
        }
        return result;
    }

    @Override
    public List<User> getAll() throws Exception {
        List<User> result = new ArrayList<>();
        try (Connection connection = JdbcProvider.getInstance().getConnection()) {
            try (Statement statement = connection.createStatement()) {
                try (ResultSet resultSet = statement.executeQuery(Query.GET_ALL.QUERY)) {
                    while (resultSet.next()) {
                        result.add(new User(
                                resultSet.getLong("id"),
                                resultSet.getString("full_name"),
                                resultSet.getString("login"),
                                resultSet.getString("password"),
                                jdbcRoleDao.get(resultSet.getLong("role_id")),
                                resultSet.getBoolean("is_active")
                        ));
                    }
                    LOGGER.info("Получены все пользователи из БД");
                }
            }
        }
        return result;
    }

    @Override
    public void update(User record) throws Exception {
        try (Connection connection = JdbcProvider.getInstance().getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(Query.UPDATE.QUERY)) {
                preparedStatement.setString(1, record.getFullName());
                preparedStatement.setString(2, record.getLogin());
                preparedStatement.setString(3, record.getPassword());
                preparedStatement.setLong(4, record.getRole().getId());
                preparedStatement.setBoolean(5, record.getIsActive());
                preparedStatement.setLong(6, record.getId());

                if (preparedStatement.executeUpdate() > 0) {
                    LOGGER.info("Был отредактирован пользователь с ID " + record.getId());
                } else {
                    LOGGER.error("Пользователь не был отредактирован!");
                }
            }
        }
    }

    @Override
    public void delete(Long id) throws Exception {
        try (Connection connection = JdbcProvider.getInstance().getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(Query.DELETE.QUERY)) {
                preparedStatement.setLong(1, id);

                if (preparedStatement.executeUpdate() > 0) {
                    LOGGER.info("Из БД удалили пользователя с ID " + id);
                } else {
                    LOGGER.error("Не получилось удалить пользователя");
                }
            }
        }
    }

    @Override
    public Long getIdByLogin(String login) throws Exception {
        Long result = null;
        try (Connection connection = JdbcProvider.getInstance().getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET_ID_BY_LOGIN.QUERY)) {
                preparedStatement.setString(1, login);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        result = resultSet.getLong("id");
                    }
                    LOGGER.info("Достали ID из БД");
                }
            }
        }
        return result;
    }

    enum Query {
        CREATE("INSERT INTO " + Query.TABLE + " (full_name, login, password, role_id, is_active) " +
                "VALUES (?, ?, ?, ?, ?);"),

        GET("SELECT full_name, login, password, role_id, is_active " +
                "FROM " + Query.TABLE + " " +
                "WHERE id = ?"),

        GET_ALL("SELECT * " +
                "FROM " + Query.TABLE),

        UPDATE("UPDATE " + Query.TABLE + " " +
                "SET full_name = ?, login = ?, password = ?, role_id = ?, is_active = ? " +
                "WHERE id = ?;"),

        DELETE("DELETE " +
                "FROM " + Query.TABLE + " " +
                "WHERE id = ?"),

        GET_ID_BY_LOGIN("SELECT id " +
                "FROM " + Query.TABLE + " " +
                "WHERE login = ?");

        private static final String TABLE = "users";

        private String QUERY;

        Query(String QUERY) {
            this.QUERY = QUERY;
        }
    }

    @Override
    public User getByLogin(String login) throws Exception {
        return null;
    }
}
