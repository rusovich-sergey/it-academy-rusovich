package by.srg.db.dao;

import by.srg.model.Model;

import java.util.List;

public interface DAO<T extends Model> {

    Long create(T record) throws Exception;

    T get(Long id) throws Exception;

    List<T> getAll() throws Exception;

    void update(T record) throws Exception;

    void delete(Long id) throws Exception;
}
