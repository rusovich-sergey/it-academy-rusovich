package by.srg.db.dao.impl.spring;

import by.srg.db.dao.StatusDao;
import by.srg.db.repository.StatusRepository;
import by.srg.model.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SpringStatusDao implements StatusDao {

    @Autowired
    private StatusRepository statusRepository;

    @Override
    public Long create(Status record) throws Exception {
        return statusRepository.save(record).getId();
    }

    @Override
    public Status get(Long id) throws Exception {
        return statusRepository.findById(id).orElseThrow(Exception::new);
    }

    @Override
    public List<Status> getAll() throws Exception {
        List<Status> result = new ArrayList<>();
        statusRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public void update(Status record) throws Exception {
        statusRepository.save(record);
    }

    @Override
    public void delete(Long id) throws Exception {
        statusRepository.deleteById(id);
    }
}
