package by.srg.db.dao.impl.hibernate;

import by.srg.db.connection.HibernateProvider;
import by.srg.db.dao.RoleDao;
import by.srg.model.Role;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class HibernateRoleDao implements RoleDao {
    private static final Logger LOGGER = LogManager.getLogger();

    private static SessionFactory sessionFactory = HibernateProvider.getSessionFactory();

    private static RoleDao roleDao;


    public HibernateRoleDao() {
    }

    public static RoleDao getInstance() {
        if (roleDao == null) {
            synchronized (HibernateRoleDao.class) {
                if (roleDao == null) {
                    roleDao = new HibernateRoleDao();
                }
            }
        }
        return roleDao;
    }

    @Override
    public Long create(Role record) throws Exception {
        return null;
    }

    @Override
    public Role get(Long id) throws Exception {
        Role result = null;

        try (Session session = sessionFactory.openSession()) {
            final CriteriaBuilder cb = session.getCriteriaBuilder();
            final CriteriaQuery<Role> query = cb.createQuery(Role.class);
            final Root<Role> root = query.from(Role.class);

            Transaction transaction = session.beginTransaction();
            query.select(root).where(cb.equal(root.get("id"), id));
            result = session.createQuery(query).uniqueResult();
            transaction.commit();
        }
        return result;
    }

    @Override
    public List<Role> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Role record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }
}
