package by.srg.db.dao.impl.spring;

import by.srg.db.dao.DocumentDao;
import by.srg.db.repository.DocumentRepository;
import by.srg.model.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SpringDocumentDao implements DocumentDao {

    @Autowired
    private DocumentRepository documentRepository;

    @Override
    public Long create(Document record) throws Exception {
        return documentRepository.save(record).getId();
    }

    @Override
    public Document get(Long id) throws Exception {
        return documentRepository.findById(id).orElseThrow(Exception::new);
    }

    @Override
    public List<Document> getAll() throws Exception {
        List<Document> result = new ArrayList<>();
        documentRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public void update(Document record) throws Exception {
        documentRepository.save(record);
    }

    @Override
    public void delete(Long id) throws Exception {
        documentRepository.deleteById(id);
    }

    @Override
    public List<Document> getAllByIdCatalog(Long id) throws Exception {
        List<Document> result = new ArrayList<>();
        documentRepository.getAllByIdCatalog(id).forEach(result::add);
        return result;
    }
}
