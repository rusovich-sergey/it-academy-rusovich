package by.srg.db.dao.impl.hibernate;

import by.srg.db.connection.HibernateProvider;
import by.srg.db.dao.BrandDao;
import by.srg.model.Brand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class HibernateBrandDao implements BrandDao {

    private static final Logger LOGGER = LogManager.getLogger();

    private static SessionFactory sessionFactory = HibernateProvider.getSessionFactory();

    private static BrandDao brandDao;

    public HibernateBrandDao() {
    }

    public static BrandDao getInstance() {
        if (brandDao == null) {
            synchronized (HibernateBrandDao.class) {
                if (brandDao == null) {
                    brandDao = new HibernateBrandDao();
                }
            }
        }
        return brandDao;
    }

    @Override
    public Long create(Brand record) throws Exception {
        Long result = null;

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            result = (Long) session.save(record);
            transaction.commit();
        }
        return result;
    }

    @Override
    public Brand get(Long id) throws Exception {
        Brand result = null;

        try (Session session = sessionFactory.openSession()) {
            final CriteriaBuilder cb = session.getCriteriaBuilder();
            final CriteriaQuery<Brand> query = cb.createQuery(Brand.class);
            final Root<Brand> root = query.from(Brand.class);

            Transaction transaction = session.beginTransaction();
            query.select(root).where(cb.equal(root.get("id"), id));
            result = session.createQuery(query).uniqueResult();
            transaction.commit();
        }
        return result;
    }

    @Override
    public List<Brand> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Brand record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }

    @Override
    public Long getIdByName(String name) throws Exception {
        Long result = null;
        try (Session session = sessionFactory.openSession()) {
            final CriteriaBuilder cb = session.getCriteriaBuilder();
            final CriteriaQuery<Brand> query = cb.createQuery(Brand.class);
            final Root<Brand> root = query.from(Brand.class);

            Transaction transaction = session.beginTransaction();
            query.select(root).where(cb.equal(root.get("name"), name));
            Brand uniqueResult = session.createQuery(query).uniqueResult();
            if (uniqueResult == null) {
                result = -1L;
            } else {
                result = uniqueResult.getId();
            }
            transaction.commit();
        }
        return result;
    }
}
