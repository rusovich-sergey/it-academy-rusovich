package by.srg.db.dao.impl.hibernate;

import by.srg.db.connection.HibernateProvider;
import by.srg.db.dao.ShopDao;
import by.srg.model.Shop;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;

public class HibernateShopDao implements ShopDao {
    private static final Logger LOGGER = LogManager.getLogger();

    private static SessionFactory sessionFactory = HibernateProvider.getSessionFactory();

    private static ShopDao shopDao;

    public HibernateShopDao() {
    }

    public static ShopDao getInstance() {
        if (shopDao == null) {
            synchronized (HibernateShopDao.class) {
                if (shopDao == null) {
                    shopDao = new HibernateShopDao();
                }
            }
        }
        return shopDao;
    }

    @Override
    public Long create(Shop record) throws Exception {
        return null;
    }

    @Override
    public Shop get(Long id) throws Exception {
        Shop result = null;

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            result = session.get(Shop.class, id);
            transaction.commit();
        }
        return result;
    }

    @Override
    public List<Shop> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Shop record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }
}
