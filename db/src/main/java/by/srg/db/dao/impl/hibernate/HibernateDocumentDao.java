package by.srg.db.dao.impl.hibernate;

import by.srg.db.connection.HibernateProvider;
import by.srg.db.dao.DocumentDao;
import by.srg.model.Document;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class HibernateDocumentDao implements DocumentDao {
    private static final Logger LOGGER = LogManager.getLogger();

    private static SessionFactory sessionFactory = HibernateProvider.getSessionFactory();

    private static DocumentDao documentDao;

    public HibernateDocumentDao() {
    }

    public static DocumentDao getInstance() {
        if (documentDao == null) {
            synchronized (HibernateDocumentDao.class) {
                if (documentDao == null) {
                    documentDao = new HibernateDocumentDao();
                }
            }
        }
        return documentDao;
    }

    @Override
    public Long create(Document record) throws Exception {
        Long result = null;

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            result = (Long) session.save(record);
            transaction.commit();
        }
        return result;
    }

    @Override
    public Document get(Long id) throws Exception {
        return null;
    }

    @Override
    public List<Document> getAll() throws Exception {
        List<Document> result = null;

        try (Session session = sessionFactory.openSession()) {
            final CriteriaBuilder cb = session.getCriteriaBuilder();
            final CriteriaQuery<Document> query = cb.createQuery(Document.class);
            final Root<Document> root = query.from(Document.class);

            Transaction transaction = session.beginTransaction();
            query.select(root);
            result = session.createQuery(query).getResultList();
            transaction.commit();
        }
        return result;
    }

    @Override
    public void update(Document record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }

    @Override
    public List<Document> getAllByIdCatalog(Long id) throws Exception {
        List<Document> result = null;

        try (Session session = sessionFactory.openSession()) {
            final CriteriaBuilder cb = session.getCriteriaBuilder();
            final CriteriaQuery<Document> query = cb.createQuery(Document.class);
            final Root<Document> root = query.from(Document.class);

            Transaction transaction = session.beginTransaction();
            query.select(root).where(cb.equal(root.get("catalog"), id));
            result = session.createQuery(query).getResultList();
            transaction.commit();
        }
        return result;
    }
}
