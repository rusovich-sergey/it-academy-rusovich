package by.srg.db.dao.impl.hibernate;

import by.srg.db.connection.HibernateProvider;
import by.srg.db.dao.UserDao;
import by.srg.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class HibernateUserDao implements UserDao {
    private static final Logger LOGGER = LogManager.getLogger();

    private static SessionFactory sessionFactory = HibernateProvider.getSessionFactory();

    private static UserDao userDao;

    public HibernateUserDao() {
    }

    public static UserDao getInstance() {
        if (userDao == null) {
            synchronized (HibernateUserDao.class) {
                if (userDao == null) {
                    userDao = new HibernateUserDao();
                }
            }
        }
        return userDao;
    }

    @Override
    public Long create(User record) throws Exception {
        Long result = null;

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            result = (Long) session.save(record);
            transaction.commit();
        }
        return result;
    }

    @Override
    public User get(Long id) throws Exception {
        User result = null;

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            result = session.get(User.class, id);
            transaction.commit();
        }
        return result;
    }

    @Override
    public List<User> getAll() throws Exception {
        List<User> result = null;

        try (Session session = sessionFactory.openSession()) {
            final CriteriaBuilder cb = session.getCriteriaBuilder();
            final CriteriaQuery<User> query = cb.createQuery(User.class);
            final Root<User> root = query.from(User.class);

            Transaction transaction = session.beginTransaction();
            query.select(root);
            result = session.createQuery(query).getResultList();
            transaction.commit();
        }
        return result;
    }

    @Override
    public void update(User record) throws Exception {

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(record);
            transaction.commit();
        }
    }

    @Override
    public void delete(Long id) throws Exception {

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(session.get(User.class, id));
            transaction.commit();
        }
    }

    @Override
    public Long getIdByLogin(String login) throws Exception {
        Long result = null;
        try (Session session = sessionFactory.openSession()) {
            final CriteriaBuilder cb = session.getCriteriaBuilder();
            final CriteriaQuery<User> query = cb.createQuery(User.class);
            final Root<User> root = query.from(User.class);

            Transaction transaction = session.beginTransaction();
            query.select(root).where(cb.equal(root.get("login"), login));
            result = session.createQuery(query).uniqueResult().getId();
            transaction.commit();
        }
        return result;
    }

    @Override
    public User getByLogin(String login) throws Exception {
        return null;
    }
}
