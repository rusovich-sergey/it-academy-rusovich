package by.srg.db.dao.impl.hibernate;

import by.srg.db.connection.HibernateProvider;
import by.srg.db.dao.ProductDao;
import by.srg.model.Product;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class HibernateProductDao implements ProductDao {

    private static final Logger LOGGER = LogManager.getLogger();

    private static SessionFactory sessionFactory = HibernateProvider.getSessionFactory();

    private static ProductDao productDao;

    public HibernateProductDao() {
    }

    public static ProductDao getInstance() {
        if (productDao == null) {
            synchronized (HibernateProductDao.class) {
                if (productDao == null) {
                    productDao = new HibernateProductDao();
                }
            }
        }
        return productDao;
    }

    @Override
    public Long create(Product record) throws Exception {
        Long result = null;

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            result = (Long) session.save(record);
            transaction.commit();
        }
        return result;
    }

    @Override
    public Product get(Long id) throws Exception {
        Product result = null;

        try (Session session = sessionFactory.openSession()) {
            final CriteriaBuilder cb = session.getCriteriaBuilder();
            final CriteriaQuery<Product> query = cb.createQuery(Product.class);
            final Root<Product> root = query.from(Product.class);

            Transaction transaction = session.beginTransaction();
            query.select(root).where(cb.equal(root.get("id"), id));
            result = session.createQuery(query).uniqueResult();
            transaction.commit();
        }
        return result;
    }

    @Override
    public List<Product> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Product record) throws Exception {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(record);
            transaction.commit();
        }
    }

    @Override
    public void delete(Long id) throws Exception {

    }

    @Override
    public Long getIdByBarcode(String barcode) throws Exception {
        Long result = null;
        try (Session session = sessionFactory.openSession()) {
            final CriteriaBuilder cb = session.getCriteriaBuilder();
            final CriteriaQuery<Product> query = cb.createQuery(Product.class);
            final Root<Product> root = query.from(Product.class);

            Transaction transaction = session.beginTransaction();
            query.select(root).where(cb.equal(root.get("barcode"), barcode));
            Product uniqueResult = session.createQuery(query).uniqueResult();
            if (uniqueResult == null) {
                result = -1L;
            } else {
                result = uniqueResult.getId();
            }
            transaction.commit();
        }
        return result;
    }
}
