package by.srg.db.dao.impl.spring;

import by.srg.db.dao.RoleDao;
import by.srg.db.repository.RoleRepository;
import by.srg.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SpringRoleDao implements RoleDao {

    @Autowired
    private RoleRepository roleRepository;

    @Override
    public Long create(Role record) throws Exception {
        return roleRepository.save(record).getId();
    }

    @Override
    public Role get(Long id) throws Exception {
        return roleRepository.findById(id).orElseThrow(Exception::new);
    }

    @Override
    public List<Role> getAll() throws Exception {
        List<Role> result = new ArrayList<>();
        roleRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public void update(Role record) throws Exception {
        roleRepository.save(record);
    }

    @Override
    public void delete(Long id) throws Exception {
        roleRepository.deleteById(id);
    }
}
