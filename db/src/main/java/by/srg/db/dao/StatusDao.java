package by.srg.db.dao;

import by.srg.model.Status;

public interface StatusDao extends DAO<Status> {

}
