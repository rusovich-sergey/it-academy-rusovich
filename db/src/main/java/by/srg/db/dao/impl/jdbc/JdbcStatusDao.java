package by.srg.db.dao.impl.jdbc;

import by.srg.db.dao.StatusDao;
import by.srg.model.Status;

import java.util.List;

public class JdbcStatusDao implements StatusDao {
    private static StatusDao statusDao;

    private JdbcStatusDao() {

    }

    public static StatusDao getInstance() {
        if (statusDao == null) {
            synchronized (JdbcStatusDao.class) {
                if (statusDao == null) {
                    statusDao = new JdbcStatusDao();
                }
            }
        }
        return statusDao;
    }

    @Override
    public Long create(Status record) throws Exception {
        return null;
    }

    @Override
    public Status get(Long id) throws Exception {
        return null;
    }

    @Override
    public List<Status> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Status record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }

    enum Query {
        GET("SELECT..."),
        INSERT("INSERT..."),
        DELETE("DELETE..."),
        UPDATE("UPDATE...");

        private static final String TABLE = "status";

        private String QUERY;

        Query(String QUERY) {
            this.QUERY = QUERY;
        }
    }
}
