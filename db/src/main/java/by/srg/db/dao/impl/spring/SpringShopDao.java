package by.srg.db.dao.impl.spring;

import by.srg.db.dao.ShopDao;
import by.srg.db.repository.ShopRepository;
import by.srg.model.Shop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SpringShopDao implements ShopDao {

    @Autowired
    private ShopRepository shopRepository;

    @Override
    public Long create(Shop record) throws Exception {
        return shopRepository.save(record).getId();
    }

    @Override
    public Shop get(Long id) throws Exception {
        return shopRepository.findById(id).orElseThrow(Exception::new);
    }

    @Override
    public List<Shop> getAll() throws Exception {
        List<Shop> result = new ArrayList<>();
        shopRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public void update(Shop record) throws Exception {
        shopRepository.save(record);
    }

    @Override
    public void delete(Long id) throws Exception {
        shopRepository.deleteById(id);
    }
}
