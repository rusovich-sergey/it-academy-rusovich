package by.srg.db.dao.impl.jdbc;

import by.srg.db.dao.ShopDao;
import by.srg.model.Shop;

import java.util.List;

public class JdbcShopDao implements ShopDao {
    private static ShopDao shopDao;

    private JdbcShopDao() {

    }

    public static ShopDao getInstance() {
        if (shopDao == null) {
            synchronized (JdbcShopDao.class) {
                if (shopDao == null) {
                    shopDao = new JdbcShopDao();
                }
            }
        }
        return shopDao;
    }

    @Override
    public Long create(Shop record) throws Exception {
        return null;
    }

    @Override
    public Shop get(Long id) throws Exception {
        return null;
    }

    @Override
    public List<Shop> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Shop record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }

    enum Query {
        GET("SELECT..."),
        INSERT("INSERT..."),
        DELETE("DELETE..."),
        UPDATE("UPDATE...");

        private static final String TABLE = "shops";

        private String QUERY;

        Query(String QUERY) {
            this.QUERY = QUERY;
        }
    }
}
