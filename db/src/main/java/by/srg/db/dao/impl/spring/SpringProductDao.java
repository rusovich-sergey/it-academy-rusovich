package by.srg.db.dao.impl.spring;

import by.srg.db.dao.ProductDao;
import by.srg.db.repository.ProductRepository;
import by.srg.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SpringProductDao implements ProductDao {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public Long create(Product record) throws Exception {
        return productRepository.save(record).getId();
    }

    @Override
    public Product get(Long id) throws Exception {
        return productRepository.findById(id).orElseThrow(Exception::new);
    }

    @Override
    public List<Product> getAll() throws Exception {
        List<Product> result = new ArrayList<>();
        productRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public void update(Product record) throws Exception {
        productRepository.save(record);
    }

    @Override
    public void delete(Long id) throws Exception {
        productRepository.deleteById(id);
    }

    @Override
    public Long getIdByBarcode(String barcode) throws Exception {
        return productRepository.getIdByBarcode(barcode);
    }
}
