package by.srg.db.dao;

import by.srg.model.User;

public interface UserDao extends DAO<User> {
    Long getIdByLogin(String login) throws Exception;

    User getByLogin(String login) throws Exception;

}
