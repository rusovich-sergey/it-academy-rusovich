package by.srg.db.dao.impl.spring;

import by.srg.db.dao.CatalogDao;
import by.srg.db.repository.CatalogRepository;
import by.srg.model.Catalog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SpringCatalogDao implements CatalogDao {

    @Autowired
    private CatalogRepository catalogRepository;

    @Override
    public Long create(Catalog record) throws Exception {
        return catalogRepository.save(record).getId();
    }

    @Override
    public Catalog get(Long id) throws Exception {
        return catalogRepository.findById(id).orElseThrow(Exception::new);
    }

    @Override
    public List<Catalog> getAll() throws Exception {
        List<Catalog> result = new ArrayList<>();
        catalogRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public void update(Catalog record) throws Exception {
        catalogRepository.save(record);
    }

    @Override
    public void delete(Long id) throws Exception {
        catalogRepository.deleteById(id);
    }
}
