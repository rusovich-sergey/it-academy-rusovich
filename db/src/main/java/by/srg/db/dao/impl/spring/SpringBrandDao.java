package by.srg.db.dao.impl.spring;

import by.srg.db.dao.BrandDao;
import by.srg.db.repository.BrandRepository;
import by.srg.model.Brand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SpringBrandDao implements BrandDao {

    @Autowired
    private BrandRepository brandRepository;

    @Override
    public Long create(Brand record) throws Exception {
        return brandRepository.save(record).getId();
    }

    @Override
    public Brand get(Long id) throws Exception {
        return brandRepository.findById(id).orElseThrow(Exception::new);
    }

    @Override
    public List<Brand> getAll() throws Exception {
        List<Brand> result = new ArrayList<>();
        brandRepository.findAll().forEach(result::add);
        return result;
    }

    @Override
    public void update(Brand record) throws Exception {
        brandRepository.save(record);
    }

    @Override
    public void delete(Long id) throws Exception {
        brandRepository.deleteById(id);
    }

    @Override
    public Long getIdByName(String name) throws Exception {
//        return brandRepository.getIdByLogin(name);
        return brandRepository.getIdByName(name);
    }
}
