package by.srg.db.dao.impl.jdbc;

import by.srg.db.connection.JdbcProvider;
import by.srg.db.dao.RoleDao;
import by.srg.model.Role;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.List;

public class JdbcRoleDao implements RoleDao {
    private static JdbcRoleDao roleDao;

    private JdbcRoleDao() {

    }

    public static JdbcRoleDao getInstance() {
        if (roleDao == null) {
            synchronized (JdbcRoleDao.class) {
                if (roleDao == null) {
                    roleDao = new JdbcRoleDao();
                }
            }
        }
        return roleDao;
    }

    @Override
    public Long create(Role record) throws Exception {
        return null;
    }

    @Override
    public Role get(Long id) throws Exception {
        Role role = null;
        try (Connection connection = JdbcProvider.getInstance().getConnection()) {
            try (PreparedStatement preparedStatement = connection.prepareStatement(Query.GET.QUERY)) {
                preparedStatement.setLong(1, id);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        role = new Role(
                                resultSet.getLong("id"),
                                resultSet.getString("name")
                        );
                    }
                }
            }
        }
        return role;
    }

    @Override
    public List<Role> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Role record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }

    enum Query {
        GET("SELECT * FROM " + Query.TABLE + " WHERE id = ?"),
        INSERT("INSERT..."),
        DELETE("DELETE..."),
        UPDATE("UPDATE...");

        private static final String TABLE = "roles";

        private String QUERY;

        Query(String QUERY) {
            this.QUERY = QUERY;
        }
    }
}
