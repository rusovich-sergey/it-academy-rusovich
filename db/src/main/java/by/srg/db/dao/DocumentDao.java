package by.srg.db.dao;

import by.srg.model.Document;

import java.util.List;

public interface DocumentDao extends DAO<Document> {
    List<Document> getAllByIdCatalog(Long id) throws Exception;

}
