package by.srg.db.dao;

import by.srg.model.Catalog;

public interface CatalogDao extends DAO<Catalog> {

}
