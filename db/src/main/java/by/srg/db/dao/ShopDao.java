package by.srg.db.dao;

import by.srg.model.Shop;

public interface ShopDao extends DAO<Shop> {

}
