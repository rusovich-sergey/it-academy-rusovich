package by.srg.db.dao.impl.jdbc;

import by.srg.db.dao.CatalogDao;
import by.srg.model.Catalog;

import java.util.List;

public class JdbcCatalogDao implements CatalogDao {
    private static CatalogDao catalogDao;

    private JdbcCatalogDao() {

    }

    public static CatalogDao getInstance() {
        if (catalogDao == null) {
            synchronized (JdbcCatalogDao.class) {
                if (catalogDao == null) {
                    catalogDao = new JdbcCatalogDao();
                }
            }
        }
        return catalogDao;
    }


    @Override
    public Long create(Catalog record) throws Exception {
        return null;
    }

    @Override
    public Catalog get(Long id) throws Exception {
        return null;
    }

    @Override
    public List<Catalog> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Catalog record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }

    enum Query {
        GET("SELECT..."),
        INSERT("INSERT..."),
        DELETE("DELETE..."),
        UPDATE("UPDATE...");

        private static final String TABLE = "catalog";

        private String QUERY;

        Query(String QUERY) {
            this.QUERY = QUERY;
        }
    }
}
