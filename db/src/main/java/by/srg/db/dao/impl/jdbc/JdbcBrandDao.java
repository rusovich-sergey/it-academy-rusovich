package by.srg.db.dao.impl.jdbc;

import by.srg.db.dao.BrandDao;
import by.srg.model.Brand;

import java.util.List;

public class JdbcBrandDao implements BrandDao {
    private static BrandDao brandDao;

    private JdbcBrandDao() {

    }

    public static BrandDao getInstance() {
        if (brandDao == null) {
            synchronized (JdbcBrandDao.class) {
                if (brandDao == null) {
                    brandDao = new JdbcBrandDao();
                }
            }
        }
        return brandDao;
    }

    @Override
    public Long create(Brand record) throws Exception {
        return null;
    }

    @Override
    public Brand get(Long id) throws Exception {
        return null;
    }

    @Override
    public List<Brand> getAll() throws Exception {
        return null;
    }

    @Override
    public void update(Brand record) throws Exception {

    }

    @Override
    public void delete(Long id) throws Exception {

    }

    @Override
    public Long getIdByName(String name) throws Exception {
        return null;
    }

    enum Query {
        GET("SELECT..."),
        INSERT("INSERT..."),
        DELETE("DELETE..."),
        UPDATE("UPDATE...");

        private static final String TABLE = "brands";

        private String QUERY;

        Query(String QUERY) {
            this.QUERY = QUERY;
        }
    }
}
