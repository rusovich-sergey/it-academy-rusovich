package by.srg.db.dao.impl.hibernate;

import by.srg.db.connection.HibernateProvider;
import by.srg.db.dao.CatalogDao;
import by.srg.model.Catalog;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class HibernateCatalogDao implements CatalogDao {
    private static final Logger LOGGER = LogManager.getLogger();

    private static SessionFactory sessionFactory = HibernateProvider.getSessionFactory();

    private static CatalogDao catalogDao;

    public HibernateCatalogDao() {
    }

    public static CatalogDao getInstance() {
        if (catalogDao == null) {
            synchronized (HibernateUserDao.class) {
                if (catalogDao == null) {
                    catalogDao = new HibernateCatalogDao();
                }
            }
        }
        return catalogDao;
    }

    @Override
    public Long create(Catalog record) throws Exception {
        Long result = null;

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            result = (Long) session.save(record);
            transaction.commit();
        }
        return result;
    }

    @Override
    public Catalog get(Long id) throws Exception {
        Catalog result = null;

        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            result = session.get(Catalog.class, id);
            transaction.commit();
        }
        return result;

    }

    @Override
    public List<Catalog> getAll() throws Exception {
        List<Catalog> result = null;

        try (Session session = sessionFactory.openSession()) {
            final CriteriaBuilder cb = session.getCriteriaBuilder();
            final CriteriaQuery<Catalog> query = cb.createQuery(Catalog.class);
            final Root<Catalog> root = query.from(Catalog.class);
            query.select(root);

            result = session.createQuery(query).getResultList();
        }
        return result;
    }

    @Override
    public void update(Catalog record) throws Exception {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.update(record);
            transaction.commit();
        }
    }

    @Override
    public void delete(Long id) throws Exception {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            session.delete(session.get(Catalog.class, id));
            transaction.commit();
        }
    }
}
