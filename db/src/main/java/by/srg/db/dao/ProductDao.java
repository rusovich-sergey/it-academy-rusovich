package by.srg.db.dao;

import by.srg.model.Product;

public interface ProductDao extends DAO<Product> {
    Long getIdByBarcode(String barcode) throws Exception;

}
